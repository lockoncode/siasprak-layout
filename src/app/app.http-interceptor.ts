import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
    HttpErrorResponse
 } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { SnackBarService } from './shared/snack-bar.service';
@Injectable()
export class AppHttpInterceptor implements  HttpInterceptor {
    constructor (
        private snackBar: SnackBarService,
        private router: Router
    ) { }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            tap(event => {
                this.snackBar.dismiss();
            }, (err: any) => {
                this.snackBar.dismiss();
                if (err instanceof HttpErrorResponse) {
                    if (err.status === 500 || err.status === 200) {
                        this.snackBar.openError('Error : Silahkan muat ulang / hubungi Admin!');
                    } else if (err.status === 401 || err.status === 403) {
                        if (err.status === 401) {
                            /* if (localStorage.getItem('id_mhs')) {
                                localStorage.removeItem('id_mhs');
                            } else if (localStorage.getItem('id_adm')) {
                                localStorage.removeItem('id_adm');
                            }*/
                            localStorage.removeItem('nama');
                            localStorage.removeItem('email');
                            localStorage.removeItem('loginAs');
                            this.snackBar.open('Silahkan login kembali', 'OK', 5000);
                        } else {
                            this.snackBar.open('Maaf, anda tidak memiliki akses', 'OK', 0);
                        }
                        /* if (localStorage.getItem('id_mhs')) {
                            localStorage.removeItem('id_mhs');
                        } else {
                            localStorage.removeItem('id_adm');
                        } */
                        this.router.navigate(['./login']);
                    } else {
                        this.snackBar.openError('404: Silahkan muat ulang browser anda');
                    }
                } else {
                    this.snackBar.open('Error : Silahkan refresh browser anda!', null, 0);
                }
            })
        );
    }
}
