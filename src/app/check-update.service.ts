import { interval } from 'rxjs';
import { Injectable } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
@Injectable()
export class CheckForUpdateService {

  constructor(updates: SwUpdate) {
    interval(6 * 60 * 60).subscribe(() => updates.checkForUpdate());
    updates.available.subscribe(event => {
      /*if (promptUser(event)) {
        updates.activateUpdate().then(() => document.location.reload());
      }*/
      updates.activateUpdate().then(() => {
        console.log('Update Available, please refresh your browser');
      });
    });
  }
}
