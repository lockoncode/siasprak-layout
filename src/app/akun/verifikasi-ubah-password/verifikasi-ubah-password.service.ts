import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { VerifiedCode, ResponseVerifiedAccount, SendVerifiedCode } from './verfikasi-ubah-password';
import { Response } from '../../shared/shared.interface';
import { LoginResponseData } from '../../login/login';

@Injectable({
  providedIn: 'root'
})
export class VerifikasiUbahPasswordService {
  public url: string = environment.apiUrl;
  constructor(
    private http: HttpClient,
    private router: Router
  ) { }
  initializeVerificationCode (VerificationCode: VerifiedCode) {
    return this.http.get<Response>(
      this.url + 'authentication/ticket/verification/'
      + VerificationCode.mode
      + '/' + VerificationCode.email
      + '/' + VerificationCode.kode_verifikasi  );
  }
  unlockWithoutNewPassword (VerificationCode: VerifiedCode) {
    return this.http.post<ResponseVerifiedAccount>(
      this.url + 'authentication/ticket/verification/action/unlock-only',
      VerificationCode
    );
  }
  sendVerificationCode (VerificationCode: SendVerifiedCode) {
    return this.http.post<ResponseVerifiedAccount>(
      this.url + 'authentication/ticket/verification/action',
      VerificationCode
    );
  }
  addLogin (data: LoginResponseData) {
    localStorage.setItem('nama', data.nama);
    localStorage.setItem('email', data.email);
    localStorage.setItem('loginAs', data.loginAs);
  }
  sendLogoutSignal () {
    return this.http.get<Response>(this.url + 'authentication/logout');
  }
  logout () {
    localStorage.removeItem('nama');
    localStorage.removeItem('email');
    localStorage.removeItem('loginAs');
    this.router.navigate(['./../login']);
  }
}
