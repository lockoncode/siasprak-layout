import { Response } from '../../shared/shared.interface';
import { LoginResponseData } from '../../login/login';
export interface VerifikasiUbahPassword {
    status: boolean;
    message: string;
    data?: {
        email: string
    };
}
export interface VerifiedCode {
    email: string;
    mode: string;
    kode_verifikasi: string;
}
export interface SendVerifiedCode extends VerifiedCode {
    clt_password: string;
    clt_password_verif: string;
}
export interface CardTitle {
    heading: string;
    subheading: string;
}
export interface ResponseVerifiedAccount extends Response {
    data: LoginResponseData;
}
