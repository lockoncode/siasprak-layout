import { TestBed, inject } from '@angular/core/testing';

import { VerifikasiUbahPasswordService } from './verifikasi-ubah-password.service';

describe('VerifikasiUbahPasswordService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VerifikasiUbahPasswordService]
    });
  });

  it('should be created', inject([VerifikasiUbahPasswordService], (service: VerifikasiUbahPasswordService) => {
    expect(service).toBeTruthy();
  }));
});
