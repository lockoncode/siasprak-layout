import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { VerifikasiUbahPasswordService } from './verifikasi-ubah-password.service';
import { VerifiedCode, CardTitle, SendVerifiedCode } from './verfikasi-ubah-password';
import { SnackBarService } from '../../shared/snack-bar.service';
import { PasswordValidation } from './password-validation';
@Component({
  selector: 'app-verifikasi-ubah-password',
  templateUrl: './verifikasi-ubah-password.component.html',
  styleUrls: ['./verifikasi-ubah-password.component.scss']
})
export class VerifikasiUbahPasswordComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private ubahPassoword: VerifikasiUbahPasswordService,
    private snackBarService: SnackBarService,
    private router: Router
  ) { }

  public formPassword: FormGroup;
  public submit: boolean;
  verifikasi: VerifiedCode = {
    mode: null,
    email: null,
    kode_verifikasi: null,
  };
  sendVerifikasi: SendVerifiedCode;
  cardTitle: CardTitle = {
    heading: null,
    subheading: null
  };
  onProgress: boolean;
  public isLockMode: boolean;
  step = {
    0: true, 1: false, 2: false, 3: false
  };
  ngOnInit() {
    this.formPassword = this.fb.group({
      clt_password : ['', [Validators.required, Validators.minLength(6)]],
      clt_password_verif : ['', [Validators.required, Validators.minLength(6)]]
    }, {
      validator: PasswordValidation.PasswordVerify
    });
    this.route.paramMap.subscribe(
      paramsID => {
        this.verifikasi.mode = paramsID.get('mode');
        this.verifikasi.email = paramsID.get('email');
        this.verifikasi.kode_verifikasi = paramsID.get('kode_verifikasi');
      }
    );
    // console.log(this.verifikasi.mode, this.verifikasi.email, this.verifikasi.kode_verifikasi);
    this.ubahPassoword.initializeVerificationCode(this.verifikasi).subscribe(
      res => {
        if (res.status) {
          this.stepActive('1');
          if (this.verifikasi.mode === 'reset') {
            this.cardTitle.heading = 'Halaman Ubah Password';
            this.cardTitle.subheading = 'Masukkan password baru anda';
          } else if (this.verifikasi.mode === 'reg') {
            this.cardTitle.heading = 'Halaman Buat Password';
            this.cardTitle.subheading = 'Silahkan masukkan password anda';
          } else if (this.verifikasi.mode === 'lock') {
            this.cardTitle.heading = 'Halaman Buka Kunci Akun';
            this.cardTitle.subheading = 'Ubah password atau lewati tahap ini';
            this.isLockMode = true;
          }
        } else {
          this.stepActive('3');
        }
      }
    );
  }
  stepActive(active: string) {
    const s = this.step;
    Object.keys(s).forEach(function(key) {
      if (active === key) {
        s[key] = true;
      } else {
        s[key] = false;
      }
    });
    this.step = s;
  }
  onSubmit () {
    if (this.formPassword.valid) {
      this.onProgress = true;
      this.sendVerifikasi = {
        mode: this.verifikasi.mode,
        email: this.verifikasi.email,
        kode_verifikasi: this.verifikasi.kode_verifikasi,
        clt_password: this.formPassword.get('clt_password').value,
        clt_password_verif: this.formPassword.get('clt_password_verif').value
      };
      this.ubahPassoword.sendVerificationCode(this.sendVerifikasi).subscribe(
        res => {
          this.onProgress = false;
          if (res.status) {
            this.ubahPassoword.addLogin(res.data);
            this.stepActive('2');
          } else {
            this.snackBarService.open(res.message, 'OK', 0);
          }
        }
      );
    } else {
      this.snackBarService.open('Lengkapi form terlebih dahulu', 'OK', 0);
    }

  }
  skipPassword () {
    if (this.isLockMode) {
      this.onProgress = true;
      this.ubahPassoword.unlockWithoutNewPassword(this.verifikasi).subscribe(
        res => {
          this.onProgress = false;
          if (res.status) {
            this.ubahPassoword.addLogin(res.data);
            this.router.navigate(['./../login']);
          } else {
            this.snackBarService.open(res.message, 'OK', 0);
          }
        }
      );
    }
  }
  doLogout () {
    this.ubahPassoword.sendLogoutSignal().subscribe(
      res => {
        if (res.status) {
          this.ubahPassoword.logout();
        } else {
          this.snackBarService.open(res.message, 'OK', 0);
        }
      }
    );
  }
  isFieldInvalid(field: string) {
    return(
      (!this.formPassword.get(field).valid && this.formPassword.get(field).touched
      )
      ||
      (this.formPassword.get(field).untouched && this.submit)
    );
}
}

