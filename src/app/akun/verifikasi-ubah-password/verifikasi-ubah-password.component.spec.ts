import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifikasiUbahPasswordComponent } from './verifikasi-ubah-password.component';

describe('VerifikasiUbahPasswordComponent', () => {
  let component: VerifikasiUbahPasswordComponent;
  let fixture: ComponentFixture<VerifikasiUbahPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifikasiUbahPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifikasiUbahPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
