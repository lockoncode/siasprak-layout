import { AbstractControl } from '@angular/forms';
export class PasswordValidation {
    static PasswordVerify (AC: AbstractControl) {
        const password = AC.get('clt_password').value;
        const password_verify = AC.get('clt_password_verif').value;
        if (password !== password_verify) {
            AC.get('clt_password_verif').setErrors({PasswordVerify: true});
        } else {
            return null;
        }
    }
}
