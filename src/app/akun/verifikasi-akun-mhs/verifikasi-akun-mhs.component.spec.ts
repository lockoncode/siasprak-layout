import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifikasiAkunMhsComponent } from './verifikasi-akun-mhs.component';

describe('VerifikasiAkunMhsComponent', () => {
  let component: VerifikasiAkunMhsComponent;
  let fixture: ComponentFixture<VerifikasiAkunMhsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifikasiAkunMhsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifikasiAkunMhsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
