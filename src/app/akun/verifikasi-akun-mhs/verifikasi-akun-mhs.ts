export interface VerifikasiAkunMhs {
    status: boolean;
    message: string;
    data?: {
        email: string
    };
}
