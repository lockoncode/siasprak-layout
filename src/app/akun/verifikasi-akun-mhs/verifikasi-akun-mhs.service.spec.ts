import { TestBed, inject } from '@angular/core/testing';

import { VerifikasiAkunMhsService } from './verifikasi-akun-mhs.service';

describe('VerifikasiAkunMhsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VerifikasiAkunMhsService]
    });
  });

  it('should be created', inject([VerifikasiAkunMhsService], (service: VerifikasiAkunMhsService) => {
    expect(service).toBeTruthy();
  }));
});
