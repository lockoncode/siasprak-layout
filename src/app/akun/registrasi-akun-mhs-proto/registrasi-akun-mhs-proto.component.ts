import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatStepper } from '@angular/material';
import { RecaptchaComponent } from 'ng-recaptcha';
// import { KatProdi } from '../registrasi-akun-mhs/registrasi-akun-mhs';
// import { RegistrasiAkunMhsService } from '../registrasi-akun-mhs/registrasi-akun-mhs.service';
import { RegistrasiAkunMhsProtoService } from './registrasi-akun-mhs-proto.service';
import { SnackBarService } from '../../shared/snack-bar.service';
import { NimValidation } from './nim-validation';
import { Prodi } from './registrasi-akun-mhs-proto';
@Component({
  selector: 'app-registrasi-akun-mhs-proto',
  templateUrl: './registrasi-akun-mhs-proto.component.html',
  styleUrls: ['./registrasi-akun-mhs-proto.component.scss']
})
export class RegistrasiAkunMhsProtoComponent implements OnInit {

  @ViewChild(RecaptchaComponent) private recaptchaComponent: RecaptchaComponent;
  public informasiMahasiswa: FormGroup;
  public informasiPribadi: FormGroup;
  public informasiKelas: FormGroup;
  public isLinear = true;
  public errorMessage = {
    status: false,
    message: 'null'
  };
  public recaptcha = {
    status: false,
    response: null
  };
  public email: string = null;
  public prodi: Prodi[];
  public angkatan: String[];

  public onPendaftaran: boolean;
  public onProgressBar: boolean;
  public onProgressSpinner = [ false, false ];
  constructor(
    private _fb: FormBuilder,
    private registrasiService: RegistrasiAkunMhsProtoService,
    private snackBar: SnackBarService
  ) { }

  ngOnInit() {
    this.onPendaftaran = true;
    this.onProgressBar = true;
    this.informasiMahasiswa = this._fb.group({
      clt_nama : ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+')]],
      clt_nim: ['', [Validators.required, Validators.pattern('^[0-9]{11,14}$')]],
      clt_nim_confirm: ['', Validators.required]
    }, {
      validator: NimValidation.NimVerification
    });
    this.informasiKelas = this._fb.group({
      clt_prodi: ['', Validators.required],
      clt_angkatan: ['', Validators.required],
      clt_kelas: ['', Validators.required]
    });
    this.informasiPribadi = this._fb.group({
      clt_email: ['', Validators.required],
      clt_hp: ['', Validators.required],
      clt_jk: ['', Validators.required]
    });
    this.registrasiService.InitData().subscribe(
      response => {
        this.prodi = response['data']['prodi'];
        this.angkatan = response['data']['angkatan'];
        this.onProgressBar = false;
      }
    );
    // http get data pendaftaran
  }

  isOtherError() {
    return this.errorMessage.status;
  }
  resolved (captchaResponse: string) {
    this.recaptcha = {
      status : true,
      response : captchaResponse
    };
  }

  cekNIM (stepper: MatStepper) {
    this.snackBar.dismiss();

    if (this.informasiMahasiswa.valid) {
      this.onProgressSpinner[0] = true;
      this.registrasiService.cekNIM(this.informasiMahasiswa.value).subscribe(
        response => {
          if (response.status) {
            stepper.next();
          } else {
            this.errorMessage = {
              status: true,
              message: response.message
            };
            this.snackBar.open(response.message, 'OK', 0);
          }
          this.onProgressSpinner[0] = false;
        }
      );
    } else {
      this.snackBar.open('Lengkapi Form pendaftaran terlebih dahulu', 'OK', 0);
    }
  }

  submitForm () {
    this.snackBar.dismiss();
    const step1 = this.informasiMahasiswa;
    const step2 = this.informasiKelas;
    const step3 = this.informasiPribadi;

    if (step1.valid && step2.valid && step3.valid) {
      if (this.recaptcha.status) {
        this.onProgressSpinner[1] = true;
        this.registrasiService.submit(step1.value, step2.value, step3.value, this.recaptcha.response).subscribe(
          response => {
            if (response.status) {
              this.onPendaftaran = false;
              this.email = this.informasiPribadi.get('clt_email').value;
            } else {
              this.recaptchaComponent.reset();
              this.recaptcha = {
                status : false,
                response : null
              };
              this.snackBar.open(response.message, 'OK', 0);
            }
            this.onProgressSpinner[1] = false;
          }
        );
      } else {
        this.snackBar.open('Silahkan isi CAPTCHA terlebih dahulu', 'OK', 0);
      }
    } else {
      this.snackBar.open('Silahkan lengkapi form terlebih dahulu', 'OK', 0);
    }
  }
  /* isFieldInvalidStep1(field: string, step: number) {
    if (step === 1) {
      return(
        (!this.informasiMahasiswa.get(field).valid && this.informasiMahasiswa.get(field).touched
          && !this.errorMessage.status
        )
        ||
        (this.informasiMahasiswa.get(field).untouched && this.submit && !this.errorMessage.status)
      );
    }
  }
*/
}
