import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrasiAkunMhsProtoComponent } from './registrasi-akun-mhs-proto.component';

describe('RegistrasiAkunMhsProtoComponent', () => {
  let component: RegistrasiAkunMhsProtoComponent;
  let fixture: ComponentFixture<RegistrasiAkunMhsProtoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrasiAkunMhsProtoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrasiAkunMhsProtoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
