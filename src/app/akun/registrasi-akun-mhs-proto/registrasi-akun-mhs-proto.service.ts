import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';
import {
  InitialDataResponse, PostCheckNIM,
  FormInfoClass, FormInfoPrivate,
  SubmitResponse, SubmitFormReg
} from './registrasi-akun-mhs-proto';
import { Response } from '../../shared/shared.interface';
@Injectable({
  providedIn: 'root'
})
export class RegistrasiAkunMhsProtoService {
  url = environment.apiUrl;
  redirectUrl: string;
  constructor(
    private http: HttpClient
  ) { }

  InitData(): Observable<InitialDataResponse> {
    return this.http.get<InitialDataResponse>(this.url + 'reg/');
  }

  cekNIM(data: PostCheckNIM): Observable<Response> {
    return this.http.post<Response>(this.url + 'reg/check/NIM', data);
  }

  submit(
    step1: PostCheckNIM,
    step2: FormInfoClass,
    step3: FormInfoPrivate,
    recaptcha: string
  ): Observable<SubmitResponse> {
    const data: SubmitFormReg = {
      clt_nama: step1.clt_nama,
      clt_nim: step1.clt_nim,
      clt_nim_confirm: step1.clt_nim_confirm,
      clt_prodi: step2.clt_prodi,
      clt_angkatan: step2.clt_angkatan,
      clt_kelas: step2.clt_kelas,
      clt_email: step3.clt_email,
      clt_hp: step3.clt_hp,
      clt_jk: step3.clt_jk,
      recaptcha: recaptcha
    };
    console.log(step2, step3);
    // const data = [step1, step2, step3, recaptcha].filter(s => !!s);
    // data.push(step1, step2, step3, recaptcha);
    return this.http.post<SubmitResponse>(this.url + 'reg/submit', data);
  }
}
