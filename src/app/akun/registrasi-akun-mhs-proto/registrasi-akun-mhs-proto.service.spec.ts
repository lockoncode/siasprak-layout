import { TestBed, inject } from '@angular/core/testing';

import { RegistrasiAkunMhsProtoService } from './registrasi-akun-mhs-proto.service';

describe('RegistrasiAkunMhsProtoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RegistrasiAkunMhsProtoService]
    });
  });

  it('should be created', inject([RegistrasiAkunMhsProtoService], (service: RegistrasiAkunMhsProtoService) => {
    expect(service).toBeTruthy();
  }));
});
