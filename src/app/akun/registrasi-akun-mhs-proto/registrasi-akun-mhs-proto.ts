import { Response } from '../../shared/shared.interface';

export interface Prodi {
    id: string;
    prodi: string;
}
export interface InitialDataResponse extends Response {
    data: {
        prodi: Prodi[];
        angkatan: string[];
    };
}
export interface SubmitResponse extends Response {
    data?: {
        expired: string;
    };
}
export interface PostCheckNIM {
    clt_nama: string;
    clt_nim: string;
    clt_nim_confirm: string;
}
export interface FormInfoClass {
    clt_prodi: string;
    clt_angkatan: string;
    clt_kelas: string;
}
export interface FormInfoPrivate {
    clt_email: string;
    clt_hp: string;
    clt_jk: string;
}
export interface SubmitFormReg extends PostCheckNIM, FormInfoClass, FormInfoPrivate {
    recaptcha: string;
}
