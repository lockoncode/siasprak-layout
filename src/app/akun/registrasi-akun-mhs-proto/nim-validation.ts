import { AbstractControl } from '@angular/forms';
export class NimValidation {
    static NimVerification (AC: AbstractControl) {
        const nim = AC.get('clt_nim').value;
        const nim_confirm = AC.get('clt_nim_confirm').value;
        if (nim !== nim_confirm) {
            AC.get('clt_nim_confirm').setErrors({NimVerify: true});
        } else {
            return null;
        }
    }
}
