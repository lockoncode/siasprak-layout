import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { SnackBarService } from '../../shared/snack-bar.service';
import { UbahPasswordAkunService } from './ubah-password-akun.service';
import { RecaptchaComponent } from 'ng-recaptcha';

import { ResetPasswordRequest } from './ubah-password-akun';
import { usernameValidation } from '../../shared/username-validation.directive';
@Component({
  selector: 'app-ubah-password-akun',
  templateUrl: './ubah-password-akun.component.html',
  styleUrls: ['ubah-password-akun.component.scss']
})
export class UbahPasswordAkunComponent implements OnInit {
  @ViewChild(RecaptchaComponent) private recaptchaComponent: RecaptchaComponent;

  constructor(
    private fb: FormBuilder,
    private snackBar: SnackBarService,
    private http: UbahPasswordAkunService,
  ) { }
  public step = {
    '1' : true, '2' : false
  };
  public errorMessage = {
    status: false,
    message: 'null'
  };
  public recaptcha = {
    status: false,
    response: null
  };
  public formData: ResetPasswordRequest = {
    clt_id: null,
    recaptcha: null
  };

  public submit: boolean;
  public onProgress:  boolean;
  public isSuccess: boolean;
  public form: FormGroup;
  public emailSend: string;
  public emailProvider: string;

  ngOnInit() {
    this.form = this.fb.group({
      clt_id : ['', [Validators.required, usernameValidation]]
    });
  }
  onSubmit() {
    if (this.recaptcha.status) {
      this.submit = true;
      if (this.form.valid) {
        this.formData = {
          clt_id: this.form.get('clt_id').value,
          recaptcha: this.recaptcha.response
        };
        this.onProgress = true;
        this.form.disable();
        this.http.reqPassword(this.formData).subscribe(
          res => {
            if (res.status) {
              this.stepActive('2');
              this.emailSend = res.data.email;
              if (res.data.email_provider) {
                this.emailProvider = 'https://' + res.data.email_provider;
              } else {
                this.emailProvider = '#';
              }
              this.isSuccess = true;
            } else {
              this.recaptchaComponent.reset();
              this.snackBar.open(res.message, 'OK', 0);
            }
            this.onProgress = false;
            this.form.enable();
          }
        );
      } else {
        this.recaptchaComponent.reset();
        this.snackBar.open('Silahkan isi ID anda terlebih dahulu', 'OK', 0);
      }
    } else {
      this.snackBar.open('Silahkan isi CAPTCHA terlebih dahulu', 'OK', 0);
    }
  }
  stepActive(active: string) {
    const s = this.step;
    Object.keys(s).forEach(function(key) {
      if (active === key) {
        s[key] = true;
      } else {
        s[key] = false;
      }
    });
    this.step = s;
  }
  isFieldInvalid(field: string) {
      return(
        (!this.form.get(field).valid && this.form.get(field).touched
          && !this.errorMessage.status
        )
        ||
        (this.form.get(field).untouched && this.submit && !this.errorMessage.status)
      );
  }
  isOtherError() {
    return this.errorMessage.status;
  }
  resolved (captchaResponse: string) {
    this.recaptcha = {
      status : true,
      response : captchaResponse
    };
    this.snackBar.dismiss();
  }

}
