import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UbahPasswordAkunComponent } from './ubah-password-akun.component';

describe('UbahPasswordAkunComponent', () => {
  let component: UbahPasswordAkunComponent;
  let fixture: ComponentFixture<UbahPasswordAkunComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UbahPasswordAkunComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UbahPasswordAkunComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
