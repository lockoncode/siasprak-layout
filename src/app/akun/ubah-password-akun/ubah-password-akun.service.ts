import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';
import { ResetPasswordResponse, ResetPasswordRequest } from './ubah-password-akun';
@Injectable({
  providedIn: 'root'
})
export class UbahPasswordAkunService {
  public url: string = environment.apiUrl;
  constructor(
    private http: HttpClient
  ) { }

  reqPassword(data: ResetPasswordRequest): Observable<ResetPasswordResponse> {
    return this.http.post<ResetPasswordResponse>(this.url + 'authentication/ticket/reset-password/request', data);
  }
}
