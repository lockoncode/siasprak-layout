export interface ResetPasswordResponse {
    status: boolean;
    message: string;
    data?: {
        email: string,
        email_provider: string
    };
}
export interface ResetPasswordRequest {
    clt_id: string;
    recaptcha: string;
}
