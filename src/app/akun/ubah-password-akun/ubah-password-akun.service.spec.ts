import { TestBed, inject } from '@angular/core/testing';

import { UbahPasswordAkunService } from './ubah-password-akun.service';

describe('UbahPasswordAkunService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UbahPasswordAkunService]
    });
  });

  it('should be created', inject([UbahPasswordAkunService], (service: UbahPasswordAkunService) => {
    expect(service).toBeTruthy();
  }));
});
