import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { RecaptchaModule } from 'ng-recaptcha';

import { registerLocaleData } from '@angular/common';
import localeid from '@angular/common/locales/id';
registerLocaleData(localeid, 'id');

import { environment } from '../environments/environment';
import { AppRoutingModule } from './app.routing.module';
import { AppMaterialModule } from './app.material.module';

import { AppComponent } from './app.component';

import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { VerifikasiAkunMhsComponent } from './akun/verifikasi-akun-mhs/verifikasi-akun-mhs.component';
import { UbahPasswordAkunComponent } from './akun/ubah-password-akun/ubah-password-akun.component';
import { VerifikasiUbahPasswordComponent } from './akun/verifikasi-ubah-password/verifikasi-ubah-password.component';

import { AppHttpInterceptor } from './app.http-interceptor';
import { RegistrasiAkunMhsProtoComponent } from './akun/registrasi-akun-mhs-proto/registrasi-akun-mhs-proto.component';
// import { FormDisabledDirective } from './shared/form-disabled.directive';

export const httpInterceptorProviders = [
  {
    provide: HTTP_INTERCEPTORS, useClass: AppHttpInterceptor, multi: true
  }
];
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    VerifikasiAkunMhsComponent,
    UbahPasswordAkunComponent,
    VerifikasiUbahPasswordComponent,
    RegistrasiAkunMhsProtoComponent,
    // FormDisabledDirective,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    // ServiceWorkerModule.register('/siasprak-layout/ngsw-worker.js', { enabled: environment.production }),
    ServiceWorkerModule.register('/siasprak/ngsw-worker.js', { enabled: environment.production }),
    AppMaterialModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpModule,
    RecaptchaModule.forRoot()
  ],
  providers: [
    httpInterceptorProviders,
    { provide: LOCALE_ID, useValue: 'id' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
