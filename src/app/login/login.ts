import { Response } from '../shared/shared.interface';
export interface LoginUsername {
    clt_id: string;
}

export interface LoginPassword {
    clt_id: string;
    clt_password: string;
}
export interface LoginResponseData {
    nama: string;
    email: string;
    loginAs: string;
}
export interface LockedLogin {
    status: boolean;
    email?: string;
    date_now?: string;
    date_expired?: string;
    count_down?: string;
}
export interface LoginResponse extends Response {
    lock?: LockedLogin;
    data?: LoginResponseData;
}
export interface LoginUsernameResponse extends Response {
    lock?: LockedLogin;
}
