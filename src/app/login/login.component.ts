import { Component, OnInit, OnChanges, AfterViewInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { animate, style, transition, trigger } from '@angular/animations';
import { timer } from 'rxjs';
import { Router } from '@angular/router';

import { LoginService } from './login.service';
import { SnackBarService } from '../shared/snack-bar.service';
import { MainService } from '../shared/main.service';
import { usernameValidation } from '../shared/username-validation.directive';

import { LoginPassword, LockedLogin } from './login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [
    trigger('loginAnimate', [
     transition(':enter', [
        style({ opacity: 0 }),
        animate('200ms', style({ opacity: '0.5' })),
        animate('600ms', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        animate('0ms', style({opacity: 0}))
      ])
    ])
  ]
})
export class LoginComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {
  formUsername: FormGroup;
  formPassword: FormGroup;
  public showLogin: boolean;
  public submit: boolean;
  public onProgress: boolean;
  public step = {
    '1' : true, '2' : false, '3': false
  };
  public locked: LockedLogin = {
    status: false,
    email: '****@****.***',
    date_now: '',
    date_expired: '',
    count_down: ''
  };
  public errorMessage = {
    status: false,
    message: 'null'
  };
  public userName: string = null;
  public lockIsSet: boolean;
  date_now: any;
  date_exp: any;
  clock = timer(0, 1000).pipe();
  subs_clock;
  hide_password = true;
  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private snackBar: SnackBarService,
    private mainService: MainService,
    private router: Router
  ) { }

  ngOnInit() {
    this.formUsername = this.fb.group({
      clt_id : ['', [Validators.required, usernameValidation]]
    });
    this.formPassword = this.fb.group({
      clt_password : ['', [Validators.required, Validators.minLength(6)]]
    });
    this.subs_clock = this.clock.subscribe( t => {
      this.timeTick();
    });
  }
  ngOnDestroy() {
    this.subs_clock.unsubscribe();
  }
  ngOnChanges() {
    this.formUsername = this.fb.group({
      userName : ['', [Validators.required, usernameValidation]]
    });
    this.formPassword = this.fb.group({
      clt_password : ['', [Validators.required, Validators.minLength(6)]]
    });
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.showLogin = true;
    }, 0);
  }
  numberPad (n) {
    return (n < 10) ? ('0' + n) : n;
  }
  timeTick() {
    // console.log('Tik');
    if (this.locked.status) {
      // console.log('Lock ON');
      if (!this.lockIsSet) {
        this.date_now = new Date(this.locked.date_now).getTime();
        this.date_exp = new Date(this.locked.date_expired).getTime();
        this.lockIsSet = true;
      } else {
        const distance = this.date_exp - this.date_now;
        const hari = Math.floor(distance / (1000 * 60 * 60 * 24));
        const jam = this.numberPad(Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)));
        const menit = this.numberPad(Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)));
        const detik = this.numberPad(Math.floor((distance % (1000 * 60)) / 1000));
        if (hari > 0 && jam > 0 && menit > 0) {
          this.locked.count_down = hari + ' Hari ' + jam + ':' + menit + ':' + detik;
        } else if ( jam > 0 && menit > 0) {
          this.locked.count_down = jam + ':' + menit + ':' + detik;
        } else {
          this.locked.count_down = menit + ':' + detik;
        }
        if (distance > 0) {
          const tempDetik = new Date(this.date_now).getSeconds();
          this.date_now = new Date (this.date_now).setSeconds(tempDetik + 1);
        } else {
          this.lockIsSet = false;
          this.locked.status = false;
          // console.log('Distance 0');
          this.stepActive('2');
        }
      }
    } else {
      // console.log('Lock OFF');
    }
  }
  onKeydown (event) {
    if (event.key === 'Enter') {
      this.formSubmit();
    }
  }
  isFieldInvalid(field: string) {
    if (this.step[1]) {
      return(
        (!this.formUsername.get(field).valid && this.formUsername.get(field).touched
          && !this.errorMessage.status
        )
        ||
        (this.formUsername.get(field).untouched && this.submit && !this.errorMessage.status)
      );
    } else if (this.step[2]) {
      return(
        (!this.formPassword.get(field).valid && this.formPassword.get(field).touched
          && !this.errorMessage.status
        )
        ||
        (this.formPassword.get(field).untouched && this.submit && !this.errorMessage.status)
      );
    } else {
      return false;
    }
  }
  stepActive(active: string) {
    const s = this.step;
    Object.keys(s).forEach(function(key) {
      if (active === key) {
        s[key] = true;
      } else {
        s[key] = false;
      }
    });
    this.step = s;
  }
  isOtherError() {
    return this.errorMessage.status;
  }
  formSubmit() {
    this.submit = true;
    let lockStatus = false;
    if (this.step[1]) {
      this.formUsername.get('clt_id').markAsDirty();
      this.formUsername.get('clt_id').markAsTouched();
      if (this.formUsername.valid) {
        this.onProgress = true;
        this.loginService.loginUsername(this.formUsername.value).subscribe(
          resp => {
            this.onProgress = false;
            if (resp['status']) {
              this.stepActive('2');
              this.formUsername.controls['clt_id'].setErrors(null);
            } else {
              if (resp['lock']['status']) {
                this.locked = resp['lock'];
                this.stepActive('3');
                lockStatus = true;
              } else {
                // normal error message
                this.errorMessage = {
                  status : true,
                  message : resp.message
                };
                this.formUsername.controls['clt_id'].setErrors({'incorect': true});
              }
            }
          }
        );
      }
    } else if (this.step[2]) {
      this.formPassword.get('clt_password').markAsDirty();
      this.formPassword.get('clt_password').markAsDirty();
      if (this.formPassword.valid) {
        this.onProgress = true;
        const loginData: LoginPassword = {
          clt_id: this.formUsername.get('clt_id').value,
          clt_password: this.formPassword.get('clt_password').value
        };
        this.loginService.loginPassword(loginData).subscribe(
          resp => {
            this.onProgress = false;
            if (resp['status']) {
              this.formPassword.controls['clt_password'].setErrors(null);
              this.loginService.addLogin(resp.data);
            } else {
              // ketika akun telah terkunci (Salah Password > 6x)
              if (resp['lock']['status']) {
                this.locked = resp['lock'];
                this.stepActive('3');
                lockStatus = true;
              } else {
                // normal error message
                this.errorMessage = {
                  status : true,
                  message : resp.message
                };
                this.formPassword.controls['clt_password'].setErrors({'incorect': true});
              }
            }
          }
        );
      }
     }
  }
}
