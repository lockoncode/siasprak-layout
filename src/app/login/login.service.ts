import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

import { environment } from '../../environments/environment';
// import { Response } from '../shared/shared.interface';
import {
  LoginUsername, LoginPassword,
  LoginResponse, LoginUsernameResponse,
  LoginResponseData
 } from './login';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  url = environment.apiUrl;
  redirectUrl: string;
  constructor(
    private http: HttpClient,
    private router: Router
  ) { }
  /**
   * New Service
   */
  loginUsername(data: LoginUsername): Observable<LoginUsernameResponse> {
    return this.http.post<LoginUsernameResponse>(this.url + 'authentication/login/check-username', data);
  }
  loginPassword(data: LoginPassword): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(this.url + 'authentication/login/check-password', data);
  }
  addLogin(data: LoginResponseData): any {
    localStorage.setItem('nama', data.nama);
    localStorage.setItem('email', data.email);
    localStorage.setItem('loginAs', data.loginAs);
    if (data.loginAs === 'MHS') {
      this.router.navigate(['mhs']);
    } else {
      this.router.navigate(['dashboard']);
    }
  }
  logout(): void {
    localStorage.removeItem('nama');
    localStorage.removeItem('email');
    localStorage.removeItem('loginAs');
    this.router.navigate(['login']);
  }
   /** */
  /* login(data: any): Observable<Login> {
    return this.http.post<Login>(this.url + 'auth/login',
    {clt_username: data.userName, clt_password : data.password});
  }
  addLogin(id: string): any {
    localStorage.setItem('id_mhs', id);
    this.router.navigate(['asisten']);
  }*/
}
