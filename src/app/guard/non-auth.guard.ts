import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanLoad } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class NonAuthGuard implements CanActivate {
  constructor (
    private router: Router
  ) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):  Observable<boolean> | Promise<boolean> | boolean  {
      if (localStorage.getItem('loginAs') && localStorage.getItem('nama') && localStorage.getItem('email')) {
        const loginState = localStorage.getItem('loginAs');
        if (loginState === 'MHS') {
          this.router.navigate(['mhs/dashboard']);
        } else if (loginState === 'ADM') {
          this.router.navigate(['dashboard']);
        }
        return false;
      }
      return true;
  }
}
