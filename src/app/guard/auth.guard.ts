import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor (
    private router: Router
  ) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if (localStorage.getItem('loginAs') && localStorage.getItem('nama') && localStorage.getItem('email')) {
        const loginState = next.data['preload'] as String;
        if (loginState === 'auth-asiten' && localStorage.getItem('loginAs') === 'MHS') {
          return true;
        } else if (loginState === 'auth-admin' && localStorage.getItem('loginAs') === 'ADM') {
          return true;
        }
      } else {
        localStorage.removeItem('loginAs');
        localStorage.removeItem('nama');
        localStorage.removeItem('email');
      }
      this.router.navigate(['login']);
      return false;
    }
}
