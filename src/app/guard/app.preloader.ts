import { PreloadingStrategy, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { of } from 'rxjs';

export class AppPreloader implements PreloadingStrategy {
    preload (route: Route, load: Function): Observable<any> {
        if (route.data) {
            if (route.data.preload === 'auth-asisten') {
                if (localStorage.getItem('id_mhs')) {
                    return load();
                }
            } else if (route.data.preload === 'auth-admin') {
                if (localStorage.getItem('id_adm')) {
                    return load();
                }
            } else if (route.data.preload === 'non-auth') {
                if (!localStorage.getItem('id_mhs') && !localStorage.getItem('id_adm')) {
                    return load();
                }
            } else {
                console.log(`Missing Route Preload Parameter.
                Please fill with "preload:auth-asisten" or "preload:auth-admin" or "preload:non-auth"`);
                return of(null);
            }
        } else {
            console.log('Missing Route Data Parameter. Please Add "preload"');
            return of(null);
        }
    }
}
