import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class SnackBarService {
  constructor(
    private snackBar: MatSnackBar
  ) { }
  open(message: string, action: string, time: number): void {
    if (time > 0) {
      this.snackBar.open(message, action, {
        duration : time
      });
    } else {
      this.snackBar.open(message, action, {
        verticalPosition : 'bottom'
      });
    }
  }
  dismiss() {
    this.snackBar.dismiss();
  }
  openError(message: string): void {
    const snackBarRef =  this.snackBar.open(message, 'Muat Ulang', {
      verticalPosition: 'bottom'
    });
    snackBarRef.onAction().subscribe(() => {
      location.reload();
    });
  }
}
