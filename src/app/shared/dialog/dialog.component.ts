import { Component, OnInit, AfterViewInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
export interface Dialog {
  title: String;
  message: String;
}
@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html'
})
export class DialogComponent implements OnInit {

  title: String;
  message: String;

  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Dialog
  ) { }

  ngOnInit() {
    this.title = this.data.title;
    this.message = this.data.message;
  }

  onClick(mode: Boolean): void {
    this.dialogRef.close(mode);
  }
}
