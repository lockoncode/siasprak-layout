import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class MainService {
  progress = new Subject<boolean>();
  prog$ = this.progress.asObservable();

  changeProg (inp: boolean) {
    this.progress.next(inp);
  }
  constructor() { }
}
