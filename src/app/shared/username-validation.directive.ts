import { AbstractControl } from '@angular/forms';

export function usernameValidation(control: AbstractControl) {
    const emailValidator = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
    const IDValidation = /^[0-9]{10,18}$/;
    if (emailValidator.test(control.value)) {
        return null;
    } else if (IDValidation.test(control.value)) {
        return null;
    }
    return {'usernameVal': {value: control.value}};
}
