import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { LayoutDataSharingService } from '../../layout/layout-data-sharing.service';
import { SnackBarService } from '../../../shared/snack-bar.service';
import { MatakuliahTambahDetailService } from './matakuliah-tambah-detail.service';
import { DetailMatakuliah, PrasyaratMK, Predikat, PeriodeKHS } from './matakuliah-tambah-detail';
import { Response } from '../../../shared/shared.interface';

@Component({
  selector: 'app-matakuliah-tambah-detail',
  templateUrl: './matakuliah-tambah-detail.component.html',
  styleUrls: ['../../pendaftaran-asisten/pendaftaran-asisten.component.scss', './matakuliah-tambah-detail.component.scss']
})
export class MatakuliahTambahDetailComponent implements OnInit, AfterViewInit {

  constructor(
    private fb: FormBuilder,
    private layoutDataSharing: LayoutDataSharingService,
    private snackBarService: SnackBarService,
    private MKService: MatakuliahTambahDetailService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  formAplikasi: FormGroup;
  detailMK: DetailMatakuliah;
  periode: string;
  onProgress: boolean;
  kodeMK: string;
  error: Response;

  prasyaratMK: PrasyaratMK;
  predikat: Predikat[];
  riwayatStudi: PeriodeKHS[];
  clt_persyaratan_id: string;
  ngOnInit() {
    this.route.paramMap.subscribe(
      paramsID => {
        this.kodeMK = paramsID.get('id');
      }
    );
    this.formAplikasi = this.fb.group({
      clt_persyaratan_nilai : ['', [Validators.required]],
      clt_persyaratan_khs : ['', [Validators.required]],
      clt_ipk: ['', [Validators.required, Validators.pattern('([0-4].[0-9][0-9])$')]]
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.progress(true);
      this.layoutDataSharing.changeTitle('Pendaftaran Asisten');
      this.MKService.getInitialData(this.kodeMK).subscribe(
        res => {
          if (res.status) {
            this.formAplikasi.patchValue(
              {'clt_ipk': res.data.ipk});
            this.detailMK = res.data.detail_mk;
            this.prasyaratMK = res.data.detail_mk.persyaratan;
            this.predikat = res.data.predikat;
            this.riwayatStudi = res.data.riwayat_studi;
            this.clt_persyaratan_id = res.data.detail_mk.persyaratan.id;
          }
          this.error = {
            status: res.status,
            message: res.message
          };
          // this.error.status = res.status;
          // this.error.message = res.message;
          this.progress(false);
        }
      );
    }, 0);
  }
  formSubmit() {
    if (this.formAplikasi.valid) {
      this.progress(true);
      const newForm = new FormData();
      newForm.append('clt_persyaratan_id', this.clt_persyaratan_id);
      newForm.append('clt_persyaratan_nilai', this.formAplikasi.get('clt_persyaratan_nilai').value);
      newForm.append('clt_persyaratan_khs', this.formAplikasi.get('clt_persyaratan_khs').value);
      newForm.append('clt_ipk', this.formAplikasi.get('clt_ipk').value);
      this.MKService.regMK(newForm, this.kodeMK).subscribe(
        res => {
          this.progress(false);
          if (res.status) {
            this.router.navigate(['../../../aplikasi-matakuliah'], { relativeTo: this.route } );
          } else {
            this.snackBarService.open(res.message, 'OK', 0);
          }
        }
      );
    } else {
      this.snackBarService.open('Gagal: Lengkapi Form terlebih dahulu!', 'OK', 5000);
    }
  }
  progress(status: boolean) {
    if (status) {
      this.layoutDataSharing.changeStateProgress(true);
      this.onProgress = true;
    } else {
      this.layoutDataSharing.changeStateProgress(false);
      this.onProgress = false;
    }
  }
}
