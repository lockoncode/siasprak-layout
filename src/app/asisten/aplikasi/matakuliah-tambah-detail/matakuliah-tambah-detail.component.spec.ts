import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatakuliahTambahDetailComponent } from './matakuliah-tambah-detail.component';

describe('MatakuliahTambahDetailComponent', () => {
  let component: MatakuliahTambahDetailComponent;
  let fixture: ComponentFixture<MatakuliahTambahDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatakuliahTambahDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatakuliahTambahDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
