import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { InitResponse } from './matakuliah-tambah-detail';
import { Response } from '../../../shared/shared.interface';
@Injectable({
  providedIn: 'root'
})
export class MatakuliahTambahDetailService {
  url = environment.apiUrl;
  constructor(
    private http: HttpClient
  ) { }

  getInitialData(KodeMK: string): Observable<InitResponse> {
    return this.http.get<InitResponse>(this.url + 'mhs/pendaftaran-asisten/aplikasi-matakuliah/list-matakuliah/' + KodeMK);
  }
  regMK(data: any, KodeMK): Observable<Response> {
    return this.http.post<Response>(this.url + 'mhs/pendaftaran-asisten/aplikasi-matakuliah/list-matakuliah/' + KodeMK, data);
  }
}
