import { Response } from '../../../shared/shared.interface';
export interface JadwalMK {
    hari: string;
    jam: string;
    ruangan: string;
    offering: string;
    prodi: string;
    angkatan: string;
    jadwal: boolean;
}
export interface PrasyaratMK {
    id: string;
    prasyarat: string;
    nilai: string;
}
export interface DetailMatakuliah {
    id: string;
    dosen: string;
    prodi: string;
    angkatan: string;
    matakuliah: string;
    persyaratan: PrasyaratMK;
}
export interface PeriodeKHS extends Response {
    semester: string;
    periode: string;
    id: string;
}
export interface Predikat {
    id: string;
    predikat: string;
}
export interface InitResponse extends Response {
    data: {
        detail_mk: DetailMatakuliah,
        riwayat_studi: PeriodeKHS[],
        predikat: Predikat[],
        jadwal: JadwalMK[]
        ipk: number;
    };
}
