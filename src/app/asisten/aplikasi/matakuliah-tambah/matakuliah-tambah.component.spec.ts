import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatakuliahTambahComponent } from './matakuliah-tambah.component';

describe('MatakuliahTambahComponent', () => {
  let component: MatakuliahTambahComponent;
  let fixture: ComponentFixture<MatakuliahTambahComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatakuliahTambahComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatakuliahTambahComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
