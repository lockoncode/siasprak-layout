import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { InitResponse } from './matakuliah-tambah';
@Injectable({
  providedIn: 'root'
})
export class MatakuliahTambahService {
  url = environment.apiUrl;
  constructor(
    private http: HttpClient
  ) { }

  getInitialData(): Observable<InitResponse> {
    return this.http.get<InitResponse>(this.url + 'mhs/pendaftaran-asisten/aplikasi-matakuliah/list-matakuliah/');
  }
}
