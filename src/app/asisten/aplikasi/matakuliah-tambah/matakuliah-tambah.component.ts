import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';

import { LayoutDataSharingService } from '../../layout/layout-data-sharing.service';
import { SnackBarService } from '../../../shared/snack-bar.service';
import { MatakuliahTambahService } from './matakuliah-tambah.service';
import { ListMatakuliah } from './matakuliah-tambah';

@Component({
  selector: 'app-matakuliah-tambah',
  templateUrl: './matakuliah-tambah.component.html',
  styleUrls: ['../../pendaftaran-asisten/pendaftaran-asisten.component.scss', './matakuliah-tambah.component.scss']
})
export class MatakuliahTambahComponent implements OnInit, AfterViewInit {

  constructor(
    private layoutDataSharing: LayoutDataSharingService,
    private snackBarService: SnackBarService,
    private MKService: MatakuliahTambahService
  ) { }
  // tableColumn: string[] = ['no', 'matakuliah', 'dosen', 'prodi', 'jadwal', 'aksi'];
  tableColumn: string[] = ['no', 'matakuliah', 'dosen', 'prodi', 'aksi'];
  listMatakuliah: ListMatakuliah[];
  dataSource = new MatTableDataSource<ListMatakuliah>(this.listMatakuliah);

  periode: string;
  onProgress: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.progress(true);
      this.layoutDataSharing.changeTitle('Pendaftaran Asisten');
      this.MKService.getInitialData().subscribe(
        res => {
          if (res.status) {
            this.dataSource.data = res.data.matakuliah;
            this.listMatakuliah = res.data.matakuliah;
            // this.dataSource = new MatTableDataSource<ListMatakuliah>(this.listMatakuliah);

          } else {
            this.snackBarService.open(res.message, 'OK', 0);
          }
          this.progress(false);
        }
      );
    }, 0);
  }
  progress(status: boolean) {
    if (status) {
      this.layoutDataSharing.changeStateProgress(true);
      this.onProgress = true;
    } else {
      this.layoutDataSharing.changeStateProgress(false);
      this.onProgress = false;
    }
  }
}
