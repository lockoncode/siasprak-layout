import { TestBed, inject } from '@angular/core/testing';

import { MatakuliahTambahService } from './matakuliah-tambah.service';

describe('MatakuliahTambahService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MatakuliahTambahService]
    });
  });

  it('should be created', inject([MatakuliahTambahService], (service: MatakuliahTambahService) => {
    expect(service).toBeTruthy();
  }));
});
