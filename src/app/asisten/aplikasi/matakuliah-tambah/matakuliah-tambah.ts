import { Response } from '../../../shared/shared.interface';
import { Matakuliah } from '../matakuliah/matakuliah';

export interface ListMatakuliah extends Matakuliah {
    jadwal: string;
    ruangan: string;
}

export interface InitResponse extends Response {
    data: {
        matakuliah: ListMatakuliah[];
    };
}
