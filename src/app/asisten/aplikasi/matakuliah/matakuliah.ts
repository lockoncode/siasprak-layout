import { Response } from '../../../shared/shared.interface';

export interface Dosen {
    nama: string;
    gelar_depan: string;
    gelar_belakang: string;
}
export interface Jadwal {
    hari: string;
    jam: string;
    kelas: string;
    ruangan: string;
    kesediaan: boolean;
}

export interface Matakuliah {
    no: number;
    id: string;
    matakuliah: string;
    dosen: Dosen;
    pesan: string;
    prodi: string;
    angkatan: string;
    jadwal: Jadwal[];
}
export interface KelengkapanPendaftaran {
    ktm: Response;
    krs: Response;
    khs: Response;
}
export interface PeriodeAsistensi {
    nama: string;
    message: string;
    isBuka: boolean;
}
export interface InitResponse extends Response {
    data: {
        matakuliah: Matakuliah[],
        periode: PeriodeAsistensi;
        kelengkapan: KelengkapanPendaftaran;
    };
}
