import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { InitResponse } from './matakuliah';
import { Response } from '../../../shared/shared.interface';
@Injectable({
  providedIn: 'root'
})
export class MatakuliahService {

  url = environment.apiUrl;
  constructor(
    private http: HttpClient
  ) { }

  getInitialData(): Observable<InitResponse> {
    return this.http.get<InitResponse>(this.url + 'mhs/pendaftaran-asisten/aplikasi-matakuliah/');
  }
  deleteData(id: string) {
    return this.http.delete<Response>(this.url + 'mhs/pendaftaran-asisten/aplikasi-matakuliah/' + id);
  }
}
