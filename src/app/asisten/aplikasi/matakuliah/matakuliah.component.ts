import { Component, OnInit, AfterViewInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';

import { LayoutDataSharingService } from '../../layout/layout-data-sharing.service';
import { SnackBarService } from '../../../shared/snack-bar.service';
import { MatakuliahService } from './matakuliah.service';
import { Matakuliah, PeriodeAsistensi, KelengkapanPendaftaran } from './matakuliah';
@Component({
  selector: 'app-matakuliah',
  templateUrl: './matakuliah.component.html',
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  styleUrls: ['../../pendaftaran-asisten/pendaftaran-asisten.component.scss', './matakuliah.component.scss']
})
export class MatakuliahComponent implements OnInit, AfterViewInit {

  constructor(
    private layoutDataSharing: LayoutDataSharingService,
    private snackBarService: SnackBarService,
    private matakuliahService: MatakuliahService
  ) { }
  // tableColumn: string[] = ['no', 'matakuliah', 'dosen', 'prodi', 'pesan', 'aksi'];
  tableColumn: string[] = ['matakuliah', 'pesan', 'aksi'];
  periode: PeriodeAsistensi;
  lampiran: KelengkapanPendaftaran;
  kelengkapan: boolean;
  progressBar: number;
  onProgress: boolean;
  listMatakuliah: Matakuliah[];
  dataSource = new MatTableDataSource<Matakuliah>(this.listMatakuliah);
  expandedElement: Matakuliah | null;
  ngOnInit() {
    this.progressBar = 10;
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.kelengkapan = false;
      this.progress(true);
      this.layoutDataSharing.changeTitle('Pendaftaran Asisten');
      this.matakuliahService.getInitialData().subscribe(
        res => {
          if (res.status) {
            if (res.data.matakuliah.length > 0) {
              this.dataSource.data = res.data.matakuliah;
              this.listMatakuliah = res.data.matakuliah;
            } else {
              this.listMatakuliah = null;
            }
            this.periode = res.data.periode;
            this.lampiran = res.data.kelengkapan;
            if (
              !this.lampiran.ktm.status ||
              !this.lampiran.krs.status ||
              !this.lampiran.khs.status ||
              !this.onProgress) {
                this.kelengkapan = true;
            }
            this.initProgressBar(res.data.kelengkapan);
          } else {
            this.snackBarService.open(res.message, 'OK', 0);
          }
          this.progress(false);
        }
      );
    }, 0);
  }
  progress(status: boolean) {
    if (status) {
      this.layoutDataSharing.changeStateProgress(true);
      this.onProgress = true;
    } else {
      this.layoutDataSharing.changeStateProgress(false);
      this.onProgress = false;
    }
  }
  initProgressBar(data: KelengkapanPendaftaran) {
    if (data.ktm.status) {
      this.progressBar += 30;
    }
    if (data.krs.status) {
      this.progressBar += 30;
    }
    if (data.khs.status) {
      this.progressBar += 30;
    }
  }
  deleteMK(id: string) {
    this.progress(true);
    this.matakuliahService.deleteData(id).subscribe(
      res => {
        if (res.status) {
          const index = this.searchMKIndex(id);
          this.listMatakuliah.splice(index, 1);
          this.dataSource = new MatTableDataSource<Matakuliah>(this.listMatakuliah);
          this.snackBarService.open(res.message, 'OK', 0);
        } else {
          this.snackBarService.open(res.message, 'OK', 0);
        }
        this.progress(false);
      }
    );
  }
  searchMKIndex(id: string) {
    let index = 0; let i = 0;
    this.listMatakuliah.forEach(element => {
      if (element.id === id) {
        index = i;
      }
      i++;
    });
    return index;
  }
  buktiPendaftaran() {
    this.snackBarService.open('Terjadi Kesalahan, Silahkan tunggu beberapa saat lagi', 'OK', 0);
  }
}
