import { TestBed, inject } from '@angular/core/testing';

import { MatakuliahService } from './matakuliah.service';

describe('MatakuliahService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MatakuliahService]
    });
  });

  it('should be created', inject([MatakuliahService], (service: MatakuliahService) => {
    expect(service).toBeTruthy();
  }));
});
