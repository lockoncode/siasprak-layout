import { NgModule } from '@angular/core';
import {
    MatIconModule,
    MatSidenavModule,
    MatMenuModule,
    MatToolbarModule,
    MatButtonModule,
    MatListModule,
    MatCardModule,
    MatTableModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatChipsModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatDialogModule
 } from '@angular/material';
 @NgModule({
    exports : [
        MatIconModule,
        MatSidenavModule,
        MatMenuModule,
        MatToolbarModule,
        MatButtonModule,
        MatListModule,
        MatCardModule,
        MatTableModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatTooltipModule,
        MatChipsModule,
        MatPaginatorModule,
        MatCheckboxModule,
        MatDialogModule
    ]
})
export class AsistenMaterialModule { }
