import { TestBed, inject } from '@angular/core/testing';

import { PengalamanOrganisasiService } from './pengalaman-organisasi.service';

describe('PengalamanOrganisasiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PengalamanOrganisasiService]
    });
  });

  it('should be created', inject([PengalamanOrganisasiService], (service: PengalamanOrganisasiService) => {
    expect(service).toBeTruthy();
  }));
});
