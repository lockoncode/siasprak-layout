import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PengalamanOrganisasiComponent } from './pengalaman-organisasi.component';

describe('PengalamanOrganisasiComponent', () => {
  let component: PengalamanOrganisasiComponent;
  let fixture: ComponentFixture<PengalamanOrganisasiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PengalamanOrganisasiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PengalamanOrganisasiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
