import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { OrganisasiResponse } from './pengalaman-organisasi';
import { Response } from '../../shared/shared.interface';
@Injectable({
  providedIn: 'root'
})
export class PengalamanOrganisasiService {
  url = environment.apiUrl;
  constructor(
    private http: HttpClient
  ) { }
  initPengalamanOrganisasi(): Observable<OrganisasiResponse> {
    return this.http.get<OrganisasiResponse>(this.url + 'mhs/pendaftaran-asisten/pengalaman-organisasi/');
  }
  uploadPengalamanOrganisasi(data): Observable<Response> {
    return this.http.post<Response>(this.url + 'mhs/pendaftaran-asisten/pengalaman-organisasi/upload', data);
  }
  deletePengalamanOrganisasi(id: string): Observable<Response> {
    return this.http.delete<Response>(this.url + 'mhs/pendaftaran-asisten/pengalaman-organisasi/' + id);
  }
}
