import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material';

import { PengalamanOrganisasiService } from './pengalaman-organisasi.service';
import { LayoutDataSharingService } from '../layout/layout-data-sharing.service';
import { SnackBarService } from '../../shared/snack-bar.service';
import { OrganisasiResponse, Organisasi } from './pengalaman-organisasi';
@Component({
  selector: 'app-pengalaman-organisasi',
  templateUrl: './pengalaman-organisasi.component.html',
  styleUrls: ['../pendaftaran-asisten/pendaftaran-asisten.component.scss', './pengalaman-organisasi.component.scss']
})
export class PengalamanOrganisasiComponent implements OnInit, AfterViewInit {

  tableColumn: string[] = ['no', 'organisasi', 'sk', 'periode', 'keterangan', 'aksi'];
  listOrganisasi: Organisasi[];
  dataSource = new MatTableDataSource<Organisasi>(this.listOrganisasi);

  formOrganisasi: FormGroup;
  file: File;
  periode: string[];

  onProgress: boolean;
  uploadButtonName: string;
  constructor(
    private fb: FormBuilder,
    private layoutDataSharing: LayoutDataSharingService,
    private pengalamanOrganisasiService: PengalamanOrganisasiService,
    private snackBarService: SnackBarService
  ) { }

  ngOnInit() {
    this.uploadButtonName = 'Upload SK/Sertifikat';
    this.formOrganisasi = this.fb.group({
      clt_organisasi : ['', [Validators.required]],
      clt_sk : ['', [Validators.required]],
      clt_periode : ['', [Validators.required]],
      upload: ['', [Validators.required]]
    });
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.progress(true);
      this.layoutDataSharing.changeTitle('Pendaftaran Asisten');
      this.getInitData();
    }, 0);
  }
  getInitData() {
    this.pengalamanOrganisasiService.initPengalamanOrganisasi().subscribe(
      res => {
        if (res.status) {
          if (res.data.organisasi.length > 0) {
            this.listOrganisasi = res.data.organisasi;
            this.dataSource.data = res.data.organisasi;
          } else {
            this.listOrganisasi = null;
          }
          this.periode = res.data.periode;
        } else {
          this.snackBarService.open(res.message, 'OK', 0);
        }
        this.progress(false);
      }
    );
  }
  progress(status: boolean) {
    if (status) {
      this.layoutDataSharing.changeStateProgress(true);
      this.onProgress = true;
      this.formOrganisasi.disable();
    } else {
      this.layoutDataSharing.changeStateProgress(false);
      this.formOrganisasi.enable();
      this.onProgress = false;
    }
  }
  onFileChange(event) {
    this.file = event.target.files[0];
    if (this.file.size < 2824188) {
      this.uploadButtonName = this.file.name;
    } else {
      this.snackBarService.open('Ukuran gambar terlalu besar, perkecil resolusi gambar', 'OK', 5000);
    }
  }
  onSubmit () {
    if (this.formOrganisasi.valid) {
      this.progress(true);
      const uploadData = new FormData();
      uploadData.append('upload', this.file, this.file.name);
      uploadData.append('clt_organisasi', this.formOrganisasi.get('clt_organisasi').value);
      uploadData.append('clt_sk', this.formOrganisasi.get('clt_sk').value);
      uploadData.append('clt_periode', this.formOrganisasi.get('clt_periode').value);

      this.pengalamanOrganisasiService.uploadPengalamanOrganisasi(uploadData).subscribe(
        res => {
          if (res.status) {
            this.snackBarService.open('Berhasil Mengupload Pengalaman Asisten', 'OK', 3000);
            this.formOrganisasi.reset();
            this.uploadButtonName = 'Upload SK/Sertifikat';
            this.getInitData();
          } else {
            this.snackBarService.open(res.message, 'OK', 0);
          }
          // this.listAsistensi = res.data.asistensi;
          this.progress(false);
        }
      );
    } else {
      this.snackBarService.open('Gagal: Lengkapi Form terlebih dahulu!', 'OK', 5000);
    }
  }
  deleteRiwayat (id: string) {
    this.progress(true);
    this.pengalamanOrganisasiService.deletePengalamanOrganisasi(id).subscribe(
      res => {
        if (res.status) {
          const index = this.searchIndex(id);
          this.dataSource.data.splice(index, 1);
          this.dataSource = new MatTableDataSource<Organisasi>(this.dataSource.data);
          this.snackBarService.open('Berhasil Menghapus Riwayat', 'OK', 0);
        } else {
          this.snackBarService.open(res.message, 'OK', 0);
        }
        this.progress(false);
      }
    );
  }
  searchIndex (id: string) {
    let index = 0; let i = 0;
    this.listOrganisasi.forEach(element => {
      if (element.id === id) {
        index = i;
      }
      i++;
    });
    return index;
  }

}
