import { Response } from '../../shared/shared.interface';

export interface Organisasi {
    id: string;
    nama: string;
    sk: string;
    periode: string;
    file_url: string;
    tanggal: string;
    keterangan: string;
    status: boolean;
}
export interface OrganisasiResponse extends Response {
    data: {
        organisasi: Organisasi[];
        periode: string[];
    };
}
