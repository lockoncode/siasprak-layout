import { Component, OnInit, OnDestroy, ChangeDetectorRef, ViewChild } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { Router } from '@angular/router';
import { LayoutDataSharingService } from './layout-data-sharing.service';
import { AsistenService } from '../asisten.service';
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, OnDestroy {
  mobileQuery: MediaQueryList;
  mainTitle: String = 'Dashboard';
  onProgress: boolean;
  private _mobileQueryListener: () => void;
  // @ViewChild('snav') sidenav: MatSidenav;
  @ViewChild('snav') public sideNav;
  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private dataSharing: LayoutDataSharingService,
    private asistenService: AsistenService,
    private router: Router
    ) {
      this.mobileQuery = media.matchMedia('(max-width: 728px)');
      this._mobileQueryListener = () => changeDetectorRef.detectChanges();
      this.mobileQuery.addListener(this._mobileQueryListener);
      this.router.events.subscribe(() => {
        if (this.mobileQuery.matches) {
          this.sideNav.close();
        }
      });
    }

  ngOnInit() {
    this.dataSharing.mainTitle.subscribe(response => this.mainTitle = response);
    this.dataSharing.isProgress.subscribe(response => this.onProgress = response);
  }
  ngOnDestroy() {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
  logout () {
    this.asistenService.logout().subscribe(
      Response => {
        if (Response.status) {
          localStorage.removeItem('nama');
          localStorage.removeItem('email');
          localStorage.removeItem('loginAs');
          this.router.navigate(['./../login']);
        }
      }
    );
  }
}
