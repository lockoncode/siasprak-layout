import { TestBed, inject } from '@angular/core/testing';

import { LayoutDataSharingService } from './layout-data-sharing.service';

describe('LayoutDataSharingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LayoutDataSharingService]
    });
  });

  it('should be created', inject([LayoutDataSharingService], (service: LayoutDataSharingService) => {
    expect(service).toBeTruthy();
  }));
});
