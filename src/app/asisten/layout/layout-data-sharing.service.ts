import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LayoutDataSharingService {
  private titleSource = new BehaviorSubject('Dashboard');
  mainTitle = this.titleSource.asObservable();

  private progressSource = new BehaviorSubject(true);
  isProgress = this.progressSource.asObservable();

  constructor() { }

  changeTitle (title: string) {
    this.titleSource.next(title);
  }
  changeStateProgress (state: boolean) {
    this.progressSource.next(state);
  }
}
