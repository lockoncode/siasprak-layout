import { TestBed, inject } from '@angular/core/testing';

import { AsistenService } from './asisten.service';

describe('AsistenService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AsistenService]
    });
  });

  it('should be created', inject([AsistenService], (service: AsistenService) => {
    expect(service).toBeTruthy();
  }));
});
