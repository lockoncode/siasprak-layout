import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';

import { AsistenRoutingModule } from './asisten.routing.module';
import { AsistenMaterialModule } from './asisten.material.module';
import { CapitalizeEachWord } from '../shared/capitalize-each-word.pipe';

import { DashboardComponent } from './dashboard/dashboard.component';
import { LayoutComponent } from './layout/layout.component';
import { PendaftaranAsistenComponent } from './pendaftaran-asisten/pendaftaran-asisten.component';
import { KelasAsistenComponent } from './kelas-asisten/kelas-asisten.component';
import { VerifikasiKtmComponent } from './verifikasi-ktm/verifikasi-ktm.component';

import { DialogComponent } from '../shared/dialog/dialog.component';
import { LayoutDataSharingService } from './layout/layout-data-sharing.service';
import { AsistenService } from './asisten.service';
import { PendaftaranAsistenService } from './pendaftaran-asisten/pendaftaran-asisten.service';
import { JadwalKuliahComponent } from './jadwal-kuliah/jadwal-kuliah.component';
import { JadwalKuliahDialogComponent } from './jadwal-kuliah/jadwal-kuliah-dialog/jadwal-kuliah-dialog-component';
import { JadwalKuliahService } from './jadwal-kuliah/jadwal-kuliah.service';
import { HasilStudiComponent } from './hasil-studi/hasil-studi.component';
import { HasilStudiService } from './hasil-studi/hasil-studi.service';
import { HasilStudiDialogComponent } from './hasil-studi/hasil-studi-dialog/hasil-studi-dialog.component';
import { PengalamanAsistenComponent } from './pengalaman-asisten/pengalaman-asisten.component';
import { PengalamanOrganisasiComponent } from './pengalaman-organisasi/pengalaman-organisasi.component';
import { MatakuliahComponent } from './aplikasi/matakuliah/matakuliah.component';
import { MatakuliahTambahComponent } from './aplikasi/matakuliah-tambah/matakuliah-tambah.component';
import { MatakuliahTambahDetailComponent } from './aplikasi/matakuliah-tambah-detail/matakuliah-tambah-detail.component';
@NgModule({
  imports: [
    CommonModule,
    AsistenMaterialModule,
    AsistenRoutingModule,
    FlexLayoutModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    JadwalKuliahComponent,
    JadwalKuliahDialogComponent,
    HasilStudiComponent,
    HasilStudiDialogComponent,
    DialogComponent
  ],
  declarations: [
    CapitalizeEachWord,
    DialogComponent,
    DashboardComponent,
    LayoutComponent,
    PendaftaranAsistenComponent,
    KelasAsistenComponent,
    VerifikasiKtmComponent,
    JadwalKuliahComponent,
    JadwalKuliahDialogComponent,
    HasilStudiComponent,
    HasilStudiDialogComponent,
    PengalamanAsistenComponent,
    PengalamanOrganisasiComponent,
    MatakuliahComponent,
    MatakuliahTambahComponent,
    MatakuliahTambahDetailComponent
  ],
  providers: [
    LayoutDataSharingService,
    AsistenService,
    PendaftaranAsistenService,
    JadwalKuliahService,
    HasilStudiService
  ]
})
export class AsistenModule { }
