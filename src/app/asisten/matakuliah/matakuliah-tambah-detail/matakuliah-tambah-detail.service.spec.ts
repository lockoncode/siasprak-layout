import { TestBed, inject } from '@angular/core/testing';

import { MatakuliahTambahDetailService } from './matakuliah-tambah-detail.service';

describe('MatakuliahTambahDetailService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MatakuliahTambahDetailService]
    });
  });

  it('should be created', inject([MatakuliahTambahDetailService], (service: MatakuliahTambahDetailService) => {
    expect(service).toBeTruthy();
  }));
});
