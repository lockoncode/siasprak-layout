import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { PendaftaranAsistenComponent } from './pendaftaran-asisten/pendaftaran-asisten.component';
import { KelasAsistenComponent } from './kelas-asisten/kelas-asisten.component';
import { VerifikasiKtmComponent } from './verifikasi-ktm/verifikasi-ktm.component';
import { HasilStudiComponent } from './hasil-studi/hasil-studi.component';
import { JadwalKuliahComponent } from './jadwal-kuliah/jadwal-kuliah.component';
import { LayoutComponent } from './layout/layout.component';
import { PengalamanAsistenComponent } from './pengalaman-asisten/pengalaman-asisten.component';
import { PengalamanOrganisasiComponent } from './pengalaman-organisasi/pengalaman-organisasi.component';
import { MatakuliahComponent } from './aplikasi/matakuliah/matakuliah.component';
import { MatakuliahTambahComponent } from './aplikasi/matakuliah-tambah/matakuliah-tambah.component';
import { MatakuliahTambahDetailComponent } from './aplikasi/matakuliah-tambah-detail/matakuliah-tambah-detail.component';

const routes: Routes = [
    {
        path: '', component: LayoutComponent,
            children: [
                /*{ path: 'dashboard', component: DashboardComponent, pathMatch: 'full' },*/
                { path: 'kelas-asisten', component: KelasAsistenComponent},
                // { path: '/dashboard', component: DashboardComponent},
                // { path: '/dashboard', redirectTo: ''},
                { path: 'pendaftaran-asisten', component: PendaftaranAsistenComponent },
                { path: 'verifikasi-ktm', component: VerifikasiKtmComponent },
                { path: 'jadwal-kuliah', component: JadwalKuliahComponent },
                { path: 'riwayat-studi', component: HasilStudiComponent },
                { path: 'pendaftaran-matakuliah', children: [
                    { path: '', component: MatakuliahComponent },
                    { path: 'tambah', component: MatakuliahTambahComponent },
                    { path: 'tambah/:id', component: MatakuliahTambahDetailComponent },
                    { path: '**', redirectTo: '' }
                ]},
                { path: 'pengalaman-asistensi', component: PengalamanAsistenComponent },
                { path: 'pengalaman-organisasi', component: PengalamanOrganisasiComponent },
                { path: '**', redirectTo: 'pendaftaran-asisten' }
            ]
    }
];

@NgModule({
    imports : [
        RouterModule.forChild(routes)
    ],
    exports : [
        RouterModule
    ],
    providers : [
    ]
})
export class AsistenRoutingModule { }
