import { Component, OnInit, AfterViewInit } from '@angular/core';
import { LayoutDataSharingService } from '../layout/layout-data-sharing.service';
import { PendaftaranAsistenService } from './pendaftaran-asisten.service';
import { SnackBarService } from '../../shared/snack-bar.service';
import { InitResponseData } from './pendaftaran-asisten';
@Component({
  selector: 'app-pendaftaran-asisten',
  templateUrl: './pendaftaran-asisten.component.html',
  styleUrls: ['./pendaftaran-asisten.component.scss']
})
export class PendaftaranAsistenComponent implements OnInit, AfterViewInit {
  constructor(
    private layoutDataSharing: LayoutDataSharingService,
    private pendaftaranAsistenService: PendaftaranAsistenService,
    private snackBar: SnackBarService
  ) { }
  isReady: boolean;
  initData: InitResponseData;
  progressBar: number;
  ngOnInit() {

  }
  ngAfterViewInit () {
    setTimeout(() => {
      this.layoutDataSharing.changeStateProgress(true);
      this.layoutDataSharing.changeTitle('Pendaftaran Asisten');
      this.progressBar = 10;
      this.pendaftaranAsistenService.initializePendaftaranAsisten().subscribe(
        res => {
          if (res.status) {
            // this.isReady = true;
            this.initData = res.data;
            this.initProgressBar(res.data);
          } else {
            this.snackBar.open(res.message, 'OK', 0);
          }
          this.layoutDataSharing.changeStateProgress(false);
        }
      );
    }, 0);
  }
  showInfo(link: string) {
    console.log(link);
  }
  initProgressBar(data: InitResponseData) {
    if (data.ktm.status) {
      this.progressBar += 30;
    }
    if (data.krs.status) {
      this.progressBar += 30;
    }
    if (data.khs.status) {
      this.progressBar += 30;
    }
  }
}
