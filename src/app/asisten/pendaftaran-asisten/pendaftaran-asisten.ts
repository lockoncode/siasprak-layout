import { Response } from '../../shared/shared.interface';

export interface InitResponseData {
    ktm: Response;
    krs: Response;
    khs: Response;
    periode: {
        nama: string,
        message: {
            message: string,
            color: string
        }
    };
}
export interface InitResponse {
    status: boolean;
    message: string;
    data: InitResponseData;
}
