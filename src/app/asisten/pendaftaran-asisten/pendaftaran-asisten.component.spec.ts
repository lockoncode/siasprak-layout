import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendaftaranAsistenComponent } from './pendaftaran-asisten.component';

describe('PendaftaranAsistenComponent', () => {
  let component: PendaftaranAsistenComponent;
  let fixture: ComponentFixture<PendaftaranAsistenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendaftaranAsistenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendaftaranAsistenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
