import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { InitResponse } from './pendaftaran-asisten';
@Injectable({
  providedIn: 'root'
})
export class PendaftaranAsistenService {
  url = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) { }

  initializePendaftaranAsisten (): Observable<InitResponse> {
    return this.http.get<InitResponse>(this.url + 'mhs/pendaftaran-asisten/');
  }
}
