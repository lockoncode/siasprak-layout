import { TestBed, inject } from '@angular/core/testing';

import { PendaftaranAsistenService } from './pendaftaran-asisten.service';

describe('PendaftaranAsistenService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PendaftaranAsistenService]
    });
  });

  it('should be created', inject([PendaftaranAsistenService], (service: PendaftaranAsistenService) => {
    expect(service).toBeTruthy();
  }));
});
