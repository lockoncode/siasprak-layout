import { Component, OnInit, AfterViewInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { JadwalKuliahService } from '../jadwal-kuliah.service';
import { SnackBarService } from '../../../shared/snack-bar.service';
import { JadwalKuliah, JadwalDialogRes } from '../jadwal-kuliah';

@Component({
    selector: 'app-jadwal-kuliah-dialog',
    templateUrl: 'jadwal-kuliah-dialog.html',
  })
  export class JadwalKuliahDialogComponent implements OnInit {
    formJadwal: FormGroup;
    listJam: number[] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    title: String = 'Tambahkan Data';
    constructor(
        public dialogRef: MatDialogRef<JadwalKuliahDialogComponent>,
        private fb: FormBuilder,
        private jadwalKuliahService: JadwalKuliahService,
        private snackBar: SnackBarService,
        @Inject(MAT_DIALOG_DATA) public data: JadwalKuliah) {}

    ngOnInit() {
        if (this.data) {
            this.title = 'Ubah Data';
            const jm = this.data.jam.split('-');
            const jmAwal: number = parseInt(jm[0], 10);
            const jmAkhir: number = parseInt(jm[1], 10);

            this.formJadwal = this.fb.group({
                clt_matakuliah : [this.data.matakuliah, [Validators.required]],
                clt_jadwal_hari : [this.data.hari, []],
                clt_jam_awal: [jmAwal, [Validators.required]],
                clt_jam_akhir: [jmAkhir, [Validators.required]],
                clt_ruangan: [this.data.ruangan, []],
                id: [this.data.id, [Validators.required]],
                mode: ['edit', [Validators.required]]
            });
        } else {
            this.formJadwal = this.fb.group({
                clt_matakuliah : ['', [Validators.required]],
                clt_jadwal_hari : ['', []],
                clt_jam_awal: [0, [Validators.required]],
                clt_jam_akhir: [0, [Validators.required]],
                clt_ruangan: ['', []],
                mode: ['add', [Validators.required]]
            });
        }
    }
    onNoClick(): void {
        this.dialogRef.close();
    }
    isEmpty(input: any): boolean {
        if (input === '' || input === null || input === undefined) {
            return true;
        }
        return false;
    }
    isFormValid(): boolean {
        const clt_ruangan = this.formJadwal.controls['clt_ruangan'];
        const clt_jadwal_hari = this.formJadwal.controls['clt_jadwal_hari'];

        if (this.formJadwal.controls['clt_jam_awal'].value > 0
            && this.formJadwal.controls['clt_jam_awal'].value > 0) {
            if (this.isEmpty(clt_ruangan.value) || this.isEmpty(clt_jadwal_hari.value)) {
                clt_ruangan.markAsTouched();
                clt_ruangan.markAsDirty();
                clt_jadwal_hari.markAsTouched();
                clt_jadwal_hari.markAsDirty();
                if (this.isEmpty(clt_ruangan.value)) {
                    clt_ruangan.setErrors({notValid: true});
                } else {
                    clt_ruangan.setErrors(null);
                }

                if (this.isEmpty(clt_jadwal_hari.value)) {
                    clt_jadwal_hari.setErrors({notValid: true});
                } else {
                    clt_jadwal_hari.setErrors(null);
                }
                return false;
            }
        } else {
            clt_ruangan.setErrors(null);
            clt_jadwal_hari.setErrors(null);
        }
        return true;
    }
    onKeydown (event: any) {
        if (event.key === 'Enter') {
            this.onSubmit();
        }
    }
    onSubmit(): void {
        let jmValid: Boolean;
        const jmAwal = this.formJadwal.controls['clt_jam_awal'];
        const jmAkhir = this.formJadwal.controls['clt_jam_akhir'];
        if ((jmAwal.value === 0 && jmAkhir.value === 0) || (jmAwal.value < jmAkhir.value)) {
            jmAwal.setErrors(null);
            jmAkhir.setErrors(null);
            jmValid = true;
        } else {
            jmAwal.setErrors({notValid: true});
            jmAkhir.setErrors({notValid: true});
            jmValid = false;
        }
        if (this.formJadwal.valid && this.isFormValid() && jmValid) {
            if (this.formJadwal.controls['mode'].value === 'add') {
                this.jadwalKuliahService.addMKData(this.formJadwal.value).subscribe(
                    res => {
                         if (res.status) {
                            const resp: JadwalDialogRes = {
                                status: res.status,
                                message: res.message,
                                action: 'add',
                                data: {
                                    id : res.data.id,
                                    jam : jmAwal.value + '-' + jmAkhir.value,
                                    matakuliah : this.formJadwal.controls['clt_matakuliah'].value,
                                    ruangan : this.formJadwal.controls['clt_ruangan'].value,
                                    hari : this.formJadwal.controls['clt_jadwal_hari'].value
                                }
                            };
                            this.dialogRef.close(resp);
                        } else {
                            this.snackBar.open(res.message, 'OK', 5000);
                        }
                    }
                );
            } else {
                this.jadwalKuliahService.editMKData(this.formJadwal.value).subscribe(
                    res => {
                        if (res.status) {
                            const resp: JadwalDialogRes = {
                                status: res.status,
                                message: res.message,
                                action: 'edit',
                                data: {
                                    id : this.formJadwal.controls['id'].value,
                                    jam : jmAwal.value + '-' + jmAkhir.value,
                                    matakuliah : this.formJadwal.controls['clt_matakuliah'].value,
                                    ruangan : this.formJadwal.controls['clt_ruangan'].value,
                                    hari : this.formJadwal.controls['clt_jadwal_hari'].value
                                }
                            };
                            this.dialogRef.close(resp);
                        } else {
                            this.snackBar.open(res.message, 'OK', 5000);
                        }
                    }
                );
            }
        } else {
            this.snackBar.open('Form tidak valid, silahkan isi kembali', 'OK', 5000);
        }
    }

}
