import { Component, OnInit, AfterViewInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { animate, style, transition, trigger } from '@angular/animations';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { JadwalKuliahService } from './jadwal-kuliah.service';
import { LayoutDataSharingService } from '../layout/layout-data-sharing.service';
import { SnackBarService } from '../../shared/snack-bar.service';
import { JadwalKuliah, JadwalDialogRes } from './jadwal-kuliah';
import { JadwalKuliahDialogComponent } from './jadwal-kuliah-dialog/jadwal-kuliah-dialog-component';
import { DialogComponent } from '../../shared/dialog/dialog.component';
@Component({
  selector: 'app-jadwal-kuliah',
  templateUrl: './jadwal-kuliah.component.html',
  styleUrls: ['../pendaftaran-asisten/pendaftaran-asisten.component.scss', './jadwal-kuliah.component.scss'],
  animations: [
    trigger('showHideAnimate', [
     transition(':enter', [
        style({ opacity: 0 }),
        animate('100ms', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ opacity: 1 }),
        animate('100ms', style({opacity: 0}))
      ])
    ])
  ]
})
export class JadwalKuliahComponent implements OnInit, AfterViewInit {

  /* tableColumn: string[] = ['no', 'matakuliah', 'jadwal', 'ruangan', 'aksi'];*/
  tableColumn: string[] = ['select', 'no', 'matakuliah', 'jadwal', 'aksi'];
  listJadwal: JadwalKuliah[];
  file: File;
  periode: string;
  onProgress: boolean;
  fileKRS: string;
  isHideDelete = false;

  selection = new SelectionModel<JadwalKuliah>(true, []);

  constructor(
    private fb: FormBuilder,
    private layoutDataSharing: LayoutDataSharingService,
    private jadwalKuliahService: JadwalKuliahService,
    private snackBarService: SnackBarService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.progress(true);
      this.layoutDataSharing.changeTitle('Pendaftaran Asisten');
      this.jadwalKuliahService.getInitialData().subscribe(
        res => {
          if (res.status) {
            if (res.data.krs.length > 0) {
              this.listJadwal = res.data.krs;
            } else {
              this.listJadwal = null;
            }
            this.fileKRS = res.data.fileKRS;
            this.periode = res.data.periode;
          } else {
            this.snackBarService.open(res.message, 'OK', 0);
          }
          this.progress(false);
        }
      );
    }, 0);
  }

  progress(status: boolean) {
    if (status) {
      this.layoutDataSharing.changeStateProgress(true);
      this.onProgress = true;
      // this.formJadwal.disable();
    } else {
      this.layoutDataSharing.changeStateProgress(false);
      // this.formJadwal.enable();
      this.onProgress = false;
    }
  }
  onFileChange(event: any) {
    this.file = event.target.files[0];
    if (this.file.size < 2824188) {
      this.progress(true);
      const uploadData = new FormData();
      uploadData.append('upload', this.file, this.file.name);
      this.jadwalKuliahService.uploadKRS(uploadData).subscribe(
        res => {
          if (res.status) {
            this.snackBarService.open(res.message, 'OK', 5000);
            this.listJadwal = res.data.krs;
            this.fileKRS = res.data.fileKRS;
          } else {
            this.snackBarService.open(res.message, 'OK', 0);
          }
          this.progress(false);
        }
      );

    } else {
      this.snackBarService.open('File terlalu besar!', 'OK', 0);
    }
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.listJadwal.length;
    return numSelected === numRows;
  }
  isChecked() {
    this.isHideDelete = this.selection.selected.length ? true : false;
  }
  eventToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.listJadwal.forEach(row => this.selection.select(row));
  }
  openDeleteConfirm(ID: string): void {
    const countData = ID ? 1 : this.selection.selected.length;
    const data = {
      title: 'Konfirmasi Hapus Data',
      message: 'Anda akan menghapus ' + countData + ' data jadwal studi anda'
    };
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '360px',
      data: data
    });
    dialogRef.afterClosed().subscribe((result: Boolean) => {
      if (result) {
        if (ID) {
          this.jadwalKuliahService.deleteMKData(ID).subscribe( res => {
            if (res.status) {
              const indx = this.listJadwal.findIndex(x => x.id === ID);
              this.listJadwal.splice(indx, 1);
              this.listJadwal = [...this.listJadwal];
              this.snackBarService.open(res.message, 'OK', 3000);
            } else {
              this.snackBarService.open(res.message, 'OK', 0);
            }
          });
        } else {
          const IDs: string[] = [];
          for (const key in this.selection.selected) {
            if (this.selection.selected[key].id) {
              IDs.push(this.selection.selected[key].id);
            }
          }

          this.jadwalKuliahService.deleteMKDatabyID(IDs).subscribe( res => {
            if (res.status) {
              res.data.forEach( (item, index) => {
                this.listJadwal.forEach( (jadwal, indx) => {
                  if (jadwal.id === item) {
                    this.listJadwal.splice(indx, 1);
                    return;
                  }
                });
              });
              this.listJadwal = [...this.listJadwal];
              this.snackBarService.open(res.message, 'OK', 3000);
            } else {
              this.snackBarService.open(res.message, 'OK', 0);
            }
          });
          this.selection.clear();
          this.isHideDelete = false;
        }
      }
    });
  }
  openDialog(data: JadwalKuliah): void {
    const dialogRef = this.dialog.open(JadwalKuliahDialogComponent, {
      width: '360px',
      data: data
    });

    dialogRef.afterClosed().subscribe((result: JadwalDialogRes) => {
      if (result) {
        if (result.action === 'add') {
          this.listJadwal.push(result.data);
          this.listJadwal = [...this.listJadwal];
        } else if (result.action === 'edit') {
          for (const key in this.listJadwal) {
            if (this.listJadwal[key]['id'] === result.data.id) {
              this.listJadwal[key] = result.data;
              break;
            }
          }
          this.listJadwal = [...this.listJadwal];
        }
      }
    });
  }
}
