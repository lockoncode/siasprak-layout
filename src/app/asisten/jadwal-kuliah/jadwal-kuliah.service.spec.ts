import { TestBed, inject } from '@angular/core/testing';

import { JadwalKuliahService } from './jadwal-kuliah.service';

describe('JadwalKuliahService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JadwalKuliahService]
    });
  });

  it('should be created', inject([JadwalKuliahService], (service: JadwalKuliahService) => {
    expect(service).toBeTruthy();
  }));
});
