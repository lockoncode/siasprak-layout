import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import {
  JadwalKuliah, JadwalKuliahResponse, AddJadwalResponse,
  DeleteByIDRes
} from './jadwal-kuliah';
import { Response } from '../../shared/shared.interface';

@Injectable({
  providedIn: 'root'
})
export class JadwalKuliahService {
  url = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) { }

  getInitialData(): Observable<JadwalKuliahResponse> {
    return this.http.get<JadwalKuliahResponse>(this.url + 'mhs/pendaftaran-asisten/krs/');
  }

  uploadKRS(krs: any): Observable<JadwalKuliahResponse> {
    return this.http.post<JadwalKuliahResponse>(this.url + 'mhs/pendaftaran-asisten/krs/upload', krs);
  }
  addMKData(data: JadwalKuliah): Observable<AddJadwalResponse> {
    return this.http.post<AddJadwalResponse>(this.url + 'mhs/pendaftaran-asisten/krs/mk', data);
  }
  editMKData(data: JadwalKuliah): Observable<Response> {
    return this.http.put<Response>(this.url + 'mhs/pendaftaran-asisten/krs/mk', data);
  }
  deleteMKData(id: string): Observable<Response> {
    return this.http.delete<Response>(this.url + 'mhs/pendaftaran-asisten/krs/mk/' + id);
  }
  deleteMKDatabyID(data: string[]): Observable<DeleteByIDRes> {
    const dataIDs = {data: data};
    return this.http.post<DeleteByIDRes>(this.url + 'mhs/pendaftaran-asisten/krs/mk/delete-by-id', dataIDs);
  }
}
