import { Response } from '../../shared/shared.interface';
export interface JadwalKuliah {
    id: string;
    matakuliah: string;
    hari: string;
    jam: string;
    ruangan: string;
}
export interface JadwalKuliahResponse extends Response {
    data: {
        krs: JadwalKuliah[],
        periode: string;
        fileKRS: string;
    };
}
export interface AddJadwalResponse extends Response {
    data: {
        id: string;
    };
}

export interface JadwalDialogRes extends Response {
    data: JadwalKuliah;
    action: string;
}
export interface DeleteByIDRes extends Response {
    data: String[];
}
