import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';
import { InitDataResponse, UploadDataResponse } from './hasil-studi';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HasilStudiService {

  url = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) { }
  initializeKHSAsisten(): Observable<InitDataResponse> {
    return this.http.get<InitDataResponse>(this.url + 'mhs/pendaftaran-asisten/khs/');
  }
  uploadKHSAsisten(data): Observable<UploadDataResponse> {
    return this.http.post<UploadDataResponse>(this.url + 'mhs/pendaftaran-asisten/khs/upload', data);
  }
}
