import { TestBed, inject } from '@angular/core/testing';

import { HasilStudiService } from './hasil-studi.service';

describe('HasiStudiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HasilStudiService]
    });
  });

  it('should be created', inject([HasilStudiService], (service: HasilStudiService) => {
    expect(service).toBeTruthy();
  }));
});
