import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HasilStudiDialogComponent } from './hasil-studi-dialog.component';

describe('HasilStudiDialogComponent', () => {
  let component: HasilStudiDialogComponent;
  let fixture: ComponentFixture<HasilStudiDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HasilStudiDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HasilStudiDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
