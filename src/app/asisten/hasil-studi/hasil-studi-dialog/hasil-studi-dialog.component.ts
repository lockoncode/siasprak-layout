import { Component, OnInit, AfterViewInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { SnackBarService } from '../../../shared/snack-bar.service';
import { HasilStudiService } from '../hasil-studi.service';
import { LayoutDataSharingService } from '../../layout/layout-data-sharing.service';
import { Semester, KHS, Dialog, DialogRes } from '../hasil-studi';
@Component({
  selector: 'app-hasil-studi-dialog',
  templateUrl: './hasil-studi-dialog.component.html',
  styleUrls: ['./hasil-studi-dialog.component.scss']
})
export class HasilStudiDialogComponent implements OnInit {

  formHasilStudi: FormGroup;
  title: String = 'Tambahkan Data';
  semester: Semester[];
  uploadButtonName: String;
  action: String = 'add';
  file: File;
  onProgress: boolean;
  isVerified: boolean;
  KHS: KHS;
  listSemester: Semester[];
  listKHS: KHS[];

  constructor(
    public dialogRef: MatDialogRef<HasilStudiDialogComponent>,
    private fb: FormBuilder,
    private hasilStudiService: HasilStudiService,
    private snackBar: SnackBarService,
    private layoutDataSharing: LayoutDataSharingService,
    @Inject(MAT_DIALOG_DATA) public data: Dialog
  ) { }

  ngOnInit() {
    // const periode = [...new Set(this.semester.map(item => item.periode))];
    /* const a = Array.from(new Set(this.semester.map((item: Semester) => item.periode)));
    console.log(a);*/
    this.uploadButtonName = 'Tambahkan KHS';

    if (this.data.KHS) {
      this.semester = this.data.semester.filter(
        (smt: Semester) => smt.existed === false || smt.semester === this.data.KHS.semester
      );
      this.KHS = this.data.KHS;
      this.action = 'edit';
      this.title = 'Ubah Data';
      this.uploadButtonName = 'Ganti KHS';
      this.isVerified = this.KHS.status_message === 2;
      this.formHasilStudi = this.fb.group({
        clt_periode : [{value: this.KHS.semester, disabled: true}, [Validators.required]],
        clt_ip : [{value: this.KHS.ip, disabled: this.isVerified}, [Validators.required, Validators.pattern('([0-4].[0-9][0-9])$')]],
        upload: ['', []]

      });
    } else {
      this.semester = this.data.semester.filter(
        (smt: Semester) => smt.existed === false
      );
      this.formHasilStudi = this.fb.group({
        clt_periode : ['', [Validators.required]],
        clt_ip : ['', [Validators.required, Validators.pattern('([0-4].[0-9][0-9])$')]],
        upload: ['', [Validators.required]]
      });
    }
  }
  progress(status: boolean) {
    if (status) {
      this.onProgress = true;
      this.layoutDataSharing.changeStateProgress(true);
      this.disabledForm(true);
    } else {
      this.onProgress = false;
      this.layoutDataSharing.changeStateProgress(false);
      this.disabledForm(false);
    }
  }
  disabledForm(bool: boolean) {
    if (bool) {
      this.formHasilStudi.disable();
    } else {
      this.formHasilStudi.enable();
    }
  }
  onSubmit () {
    if (this.formHasilStudi.valid) {
      this.progress(true);
      const uploadData = new FormData();
      if (this.file) {
        uploadData.append('upload', this.file, this.file.name);
      }
      uploadData.append('clt_ip', this.formHasilStudi.get('clt_ip').value);
      uploadData.append('clt_semester', this.formHasilStudi.get('clt_periode').value);

      this.hasilStudiService.uploadKHSAsisten(uploadData).subscribe(
        res => {
          if (res.status) {
            this.snackBar.open(res.message, 'OK', 3000);
            this.formHasilStudi.reset();

            const resp: DialogRes = {
              KHS: res.data.khs,
              semester: res.data.semester,
              action: this.action
            };
            this.dialogRef.close(resp);

            this.uploadButtonName = 'Upload KHS';
          } else {
            this.snackBar.open(res.message, 'OK', 0);
          }
          this.listKHS = res.data.khs;
          this.listSemester = res.data.semester;
          this.progress(false);
        }
      );
    } else {
      this.snackBar.open('Gagal: Lengkapi Form terlebih dahulu!', 'OK', 5000);
    }
  }
  onFileChange(event: any) {
    this.file = event.target.files[0];
    if (this.file.size < 2824188) {
      this.uploadButtonName = this.file.name;
    } else {
      this.snackBar.open('Ukuran gambar terlalu besar, perkecil resolusi gambar', 'OK', 5000);
    }
  }
  onNoClick() {
    this.dialogRef.close(false);
  }
}
