import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HasilStudiComponent } from './hasil-studi.component';

describe('HasilStudiComponent', () => {
  let component: HasilStudiComponent;
  let fixture: ComponentFixture<HasilStudiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HasilStudiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HasilStudiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
