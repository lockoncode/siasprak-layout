import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { HasilStudiDialogComponent } from './hasil-studi-dialog/hasil-studi-dialog.component';
import { HasilStudiService } from './hasil-studi.service';
import { LayoutDataSharingService } from '../layout/layout-data-sharing.service';
import { SnackBarService } from '../../shared/snack-bar.service';
import { Semester, KHS, Dialog, DialogRes } from './hasil-studi';
@Component({
  selector: 'app-hasil-studi',
  templateUrl: './hasil-studi.component.html',
  styleUrls: ['../pendaftaran-asisten/pendaftaran-asisten.component.scss', './hasil-studi.component.scss']
})
export class HasilStudiComponent implements OnInit, AfterViewInit {

  constructor(
    private fb: FormBuilder,
    private layoutDataSharing: LayoutDataSharingService,
    private hasilStudiService: HasilStudiService,
    private snackBarService: SnackBarService,
    public dialog: MatDialog
  ) { }
  // tableColumn: string[] = ['no', 'semester', 'periode', 'ip', 'tanggal', 'status', 'aksi'];
  tableColumn: string[] = ['no', 'semester', 'ip', 'periode', 'status', 'aksi'];
  formUpload: FormGroup;
  file: File;
  listSemester: Semester[];
  listKHS: KHS[];
  onProgress: boolean;
  uploadButtonName: string;
  ngOnInit() {
    this.uploadButtonName = 'Upload KHS';
    this.formUpload = this.fb.group({
      clt_semester : ['', [Validators.required]],
      clt_ip : ['', [Validators.required, Validators.pattern('([0-4].[0-9][0-9])$')]],
      upload: ['', [Validators.required]]
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.progress(true);
      this.layoutDataSharing.changeTitle('Pendaftaran Asisten');
      this.hasilStudiService.initializeKHSAsisten().subscribe(
        res => {
          if (res.status) {
            if (res.data.khs.length > 0) {
              this.listKHS = res.data.khs;
            } else {
              this.listKHS = null;
            }
            this.listSemester = res.data.semester;
          } else {
            this.snackBarService.open(res.message, 'OK', 0);
          }
          this.progress(false);
        }
      );
    }, 0);
  }
  disabledForm(bool: boolean) {
    if (bool) {
      this.formUpload.disable();
    } else {
      this.formUpload.enable();
    }
  }
  progress(status: boolean) {
    if (status) {
      this.onProgress = true;
      this.layoutDataSharing.changeStateProgress(true);
      this.disabledForm(true);
    } else {
      this.onProgress = false;
      this.layoutDataSharing.changeStateProgress(false);
      this.disabledForm(false);
    }
  }
  onFileChange(event) {
    this.file = event.target.files[0];
    if (this.file.size < 2824188) {
      this.uploadButtonName = this.file.name;
    } else {
      this.snackBarService.open('Ukuran gambar terlalu besar, perkecil resolusi gambar', 'OK', 5000);
    }
  }
  onSubmit () {
    if (this.formUpload.valid) {
      this.progress(true);
      const uploadData = new FormData();
      uploadData.append('upload', this.file, this.file.name);
      uploadData.append('clt_ip', this.formUpload.get('clt_ip').value);
      uploadData.append('clt_semester', this.formUpload.get('clt_semester').value);

      this.hasilStudiService.uploadKHSAsisten(uploadData).subscribe(
        res => {
          if (res.status) {
            this.snackBarService.open('Berhasil Mengupload KHS Semester ' + this.formUpload.get('clt_semester').value, 'OK', 3000);
            this.formUpload.reset();
            this.uploadButtonName = 'Upload KHS';
          } else {
            this.snackBarService.open(res.message, 'OK', 0);
          }
          this.listKHS = res.data.khs;
          this.listSemester = res.data.semester;
          this.progress(false);
        }
      );
    } else {
      this.snackBarService.open('Gagal: Lengkapi Form terlebih dahulu!', 'OK', 5000);
    }
  }
  openDialog(data: Dialog): void {
    const dialogRef = this.dialog.open(HasilStudiDialogComponent, {
      width: '360px',
      data: {KHS : data, semester: this.listSemester}
    });
    dialogRef.afterClosed().subscribe((result: DialogRes) => {
      if (result) {
        if (result.action === 'add') {
          this.listKHS = [...result.KHS];
          this.listSemester = [...result.semester];
        } else {
          this.listKHS = [...result.KHS];
        }
      }
    });
  }
}
