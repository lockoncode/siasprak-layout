import { Response } from '../../shared/shared.interface';

export interface KHS {
    no: number;
    id: string;
    semester: string;
    tahun_ajaran: string;
    ip: number;
    status_setuju: boolean;
    status: boolean;
    file_url: string;
    status_message: number;
}
/* export interface Semester {
    id: number;
    label: string;
    periode: string;
    disabled: boolean;
} */
export interface Semester {
    semester: string;
    tahun: string;
    periode: string;
    existed: boolean;
}
export interface Dialog {
    KHS: KHS;
    semester: Semester[];
}
export interface DialogRes {
    KHS: KHS[];
    semester: Semester[];
    action: String;
}
export interface InitDataResponse extends Response {
    data: {
        semester: Semester[],
        khs: KHS[],
        periode: string
    };
}
export interface UploadDataResponse extends Response {
    data: {
        semester: Semester[],
        khs: KHS[]
    };
}
