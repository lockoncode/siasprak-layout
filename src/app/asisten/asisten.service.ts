import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
export interface LogoutResponse {
  status: boolean;
  message: string;
}
@Injectable({
  providedIn: 'root'
})
export class AsistenService {
  url = environment.apiUrl;
  constructor(
    private http: HttpClient
  ) { }
  logout (): Observable<LogoutResponse> {
      return this.http.get<LogoutResponse>(this.url + 'authentication/logout');
  }
}
