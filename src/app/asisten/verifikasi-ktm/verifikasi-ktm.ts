import { Response } from '../../shared/shared.interface';

export interface FormData {
    clt_nama: string;
    clt_nim: string;
    clt_prodi: string;
}

export interface ListProdi {
    id: string;
    nama: string;
}

export interface KTM extends Response {
    imageUrl: string;
    is_verified: boolean;
}

export interface InitDataResponse extends Response {
    data: {
        formData: FormData,
        prodi: ListProdi,
        ktm: KTM
    };
}

export interface FormSubmitResponse extends Response {
    data: {
        prodi: ListProdi,
        ktm: KTM
    };
}

export interface UploadKTMResponse extends Response {
    data: {
        prodi: ListProdi,
        ktm: KTM,
        formData: FormData
    };
}
/*export interface FormData {
    clt_nama: string;
    clt_nim: string;
    clt_prodi: string;
}
export interface Data {
    is_verified: boolean;
    message: string;
}
export interface Prodi {
    id: string;
    nama: string;
}
export interface InitDataResponse extends Response {
    formData: FormData;
    data: Data;
    prodi: Prodi;
}
export interface FormSubmitResponse extends Response {
    is_verified: boolean;
}
export interface UploadKTMResponse extends Response {
    data: {
        imageUrl: string,
        is_verified: boolean,
        message: string,
        date: string
    };
}
*/
