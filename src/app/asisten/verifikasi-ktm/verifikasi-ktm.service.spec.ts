import { TestBed, inject } from '@angular/core/testing';

import { VerifikasiKTMService } from './verifikasi-ktm.service';

describe('VerifikasiKTMService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VerifikasiKTMService]
    });
  });

  it('should be created', inject([VerifikasiKTMService], (service: VerifikasiKTMService) => {
    expect(service).toBeTruthy();
  }));
});
