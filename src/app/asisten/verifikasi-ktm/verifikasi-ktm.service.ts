import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';
import { InitDataResponse, FormData, FormSubmitResponse , UploadKTMResponse} from './verifikasi-ktm';

@Injectable({
  providedIn: 'root'
})
export class VerifikasiKTMService {
  url = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) { }

  initializeIdentitasAsisten (): Observable<InitDataResponse> {
    return this.http.get<InitDataResponse>(this.url + 'mhs/pendaftaran-asisten/verifikasi-ktm/');
  }
  submitForm (data: FormData): Observable<FormSubmitResponse> {
    return this.http.post<FormSubmitResponse>(this.url + 'mhs/pendaftaran-asisten/verifikasi-ktm/perbarui-identitas', data);
  }
  uploadKTM (form: any): Observable<UploadKTMResponse> {
    return this.http.post<UploadKTMResponse>(this.url + 'mhs/pendaftaran-asisten/verifikasi-ktm/upload-ktm', form);
  }
}
