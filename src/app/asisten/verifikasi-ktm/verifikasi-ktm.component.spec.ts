import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifikasiKtmComponent } from './verifikasi-ktm.component';

describe('VerifikasiKtmComponent', () => {
  let component: VerifikasiKtmComponent;
  let fixture: ComponentFixture<VerifikasiKtmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifikasiKtmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifikasiKtmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
