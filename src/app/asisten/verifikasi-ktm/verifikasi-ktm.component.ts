import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { VerifikasiKTMService } from './verifikasi-ktm.service';
import { LayoutDataSharingService } from '../layout/layout-data-sharing.service';
import { SnackBarService } from '../../shared/snack-bar.service';
import { ListProdi, FormData, KTM } from './verifikasi-ktm';
@Component({
  selector: 'app-verifikasi-ktm',
  templateUrl: './verifikasi-ktm.component.html',
  styleUrls: ['../pendaftaran-asisten/pendaftaran-asisten.component.scss', './verifikasi-ktm.component.scss']
})
export class VerifikasiKtmComponent implements OnInit, AfterViewInit {

  constructor(
    private fb: FormBuilder,
    private layoutDataSharing: LayoutDataSharingService,
    private verifikasiKTMService: VerifikasiKTMService,
    private snackBarService: SnackBarService
  ) { }

  formIdentitas: FormGroup;
  backupForm: FormData;
  ktm: KTM;
  fileKTM: File;

  prodi: ListProdi;
  public onProgress: boolean;
  ngOnInit() {
    this.formIdentitas = this.fb.group({
      clt_nama : ['', [Validators.required]],
      clt_nim : ['', [Validators.required]],
      clt_prodi : ['', [Validators.required]]
    });
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.progress(true);
      this.layoutDataSharing.changeTitle('Pendaftaran Asisten');
      this.verifikasiKTMService.initializeIdentitasAsisten().subscribe(
        res => {
          if (res.status) {
            this.formIdentitas.setValue({
              clt_nama: res.data.formData.clt_nama,
              clt_nim: res.data.formData.clt_nim,
              clt_prodi: res.data.formData.clt_prodi
            });
            this.backupForm = res.data.formData;
            this.ktm = res.data.ktm;
            this.disabledForm(res.data.ktm.is_verified);
            this.prodi = res.data.prodi;
          } else {
            this.snackBarService.open(res.message, 'OK', 0);
          }
          this.progress(false);
        }
      );
    }, 0);
  }
  onSubmit() {
    if (this.formIdentitas.valid) {
      const isNotChange = this.isFormNotChange();
      if (!isNotChange) {
        this.progress(true);
        this.verifikasiKTMService.submitForm(this.formIdentitas.value).subscribe(
          res => {
            if (res.status) {
              this.backupForm = this.formIdentitas.value;
              this.snackBarService.open(res.message, 'OK', 5000);
            } else {
              this.snackBarService.open(res.message, 'OK', 0);
              if (res.data.ktm.is_verified) {
                this.formIdentitas.setValue({
                  clt_nama: this.backupForm.clt_nama,
                  clt_nim: this.backupForm.clt_nim,
                  clt_prodi: this.backupForm.clt_prodi
                });
              }
            }
            this.disabledForm(res.data.ktm.is_verified);
            this.prodi = res.data.prodi;
            this.ktm = res.data.ktm;
            this.progress(false);
          }
        );
      }
    }
  }
  onFileChange(event) {
    this.fileKTM = event.target.files[0];
    if (this.fileKTM.size < 2824188) {
      this.progress(true);
      const uploadData = new FormData();
      uploadData.append('upload', this.fileKTM, this.fileKTM.name);
      this.verifikasiKTMService.uploadKTM(uploadData).subscribe(
        res => {
          if (res.status) {
            this.snackBarService.open(res.message, 'OK', 5000);
          } else {
            this.snackBarService.open(res.message, 'OK', 0);
          }
          this.progress(false);
          this.disabledForm(res.data.ktm.is_verified);
          this.formIdentitas.setValue({
            clt_nama: res.data.formData.clt_nama,
            clt_nim: res.data.formData.clt_nim,
            clt_prodi: res.data.formData.clt_prodi
          });
          this.ktm = res.data.ktm;
          this.prodi = res.data.prodi;
        }
      );

    } else {
      this.snackBarService.open('Ukuran gambar terlalu besar, perkecil resolusi gambar', 'OK', 0);
    }
  }
  progress(status: boolean) {
    if (status) {
      this.onProgress = true;
      this.layoutDataSharing.changeStateProgress(true);
      this.disabledForm(true);
    } else {
      this.onProgress = false;
      this.layoutDataSharing.changeStateProgress(false);
    }
  }
  isFormNotChange () {
    const clt_nama = this.backupForm.clt_nama === this.formIdentitas.get('clt_nama').value;
    const clt_nim = this.backupForm.clt_nim === this.formIdentitas.get('clt_nim').value;
    const clt_prodi = this.backupForm.clt_prodi === this.formIdentitas.get('clt_prodi').value;
    return clt_nama && clt_nim && clt_prodi;
  }
  disabledForm(bool: boolean) {
    if (bool) {
      this.formIdentitas.disable();
    } else {
      this.formIdentitas.enable();
    }
  }



}
