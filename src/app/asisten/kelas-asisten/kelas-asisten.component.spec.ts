import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KelasAsistenComponent } from './kelas-asisten.component';

describe('KelasAsistenComponent', () => {
  let component: KelasAsistenComponent;
  let fixture: ComponentFixture<KelasAsistenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KelasAsistenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KelasAsistenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
