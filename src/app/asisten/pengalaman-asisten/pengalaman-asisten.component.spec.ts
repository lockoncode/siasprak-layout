import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PengalamanAsistenComponent } from './pengalaman-asisten.component';

describe('PengalamanAsistenComponent', () => {
  let component: PengalamanAsistenComponent;
  let fixture: ComponentFixture<PengalamanAsistenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PengalamanAsistenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PengalamanAsistenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
