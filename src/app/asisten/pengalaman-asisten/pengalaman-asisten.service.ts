import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { AsistensiResponse } from './pengalaman-asisten';
@Injectable({
  providedIn: 'root'
})
export class PengalamanAsistenService {
  url = environment.apiUrl;
  constructor(
    private http: HttpClient
  ) { }
  initPengalamanAsisten(): Observable<AsistensiResponse> {
    return this.http.get<AsistensiResponse>(this.url + 'mhs/pendaftaran-asisten/pengalaman-asistensi/');
  }
  uploadPengalamanAsisten(data): Observable<AsistensiResponse> {
    return this.http.post<AsistensiResponse>(this.url + 'mhs/pendaftaran-asisten/pengalaman-asistensi/upload', data);
  }
  deletePengalamanAsisten(id: string): Observable<AsistensiResponse> {
    return this.http.delete<AsistensiResponse>(this.url + 'mhs/pendaftaran-asisten/pengalaman-asistensi/' + id);
  }
}
