import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material';

import { PengalamanAsistenService } from './pengalaman-asisten.service';
import { LayoutDataSharingService } from '../layout/layout-data-sharing.service';
import { SnackBarService } from '../../shared/snack-bar.service';
import { Asistensi, PeriodeAsisten } from './pengalaman-asisten';

@Component({
  selector: 'app-pengalaman-asisten',
  templateUrl: './pengalaman-asisten.component.html',
  styleUrls: ['../pendaftaran-asisten/pendaftaran-asisten.component.scss', './pengalaman-asisten.component.scss']
})
export class PengalamanAsistenComponent implements OnInit, AfterViewInit {

  tableColumn: string[] = ['no', 'matakuliah', 'periode', 'tanggal', 'status', 'aksi'];
  listAsistensi: Asistensi[];
  dataSource = new MatTableDataSource<Asistensi>(this.listAsistensi);
  formAsistensi: FormGroup;
  file: File;
  periodeAsisten: PeriodeAsisten[];
  onProgress: boolean;
  uploadButtonName: string;

  constructor(
    private fb: FormBuilder,
    private layoutDataSharing: LayoutDataSharingService,
    private pengalamanAsistenService: PengalamanAsistenService,
    private snackBarService: SnackBarService
  ) { }

  ngOnInit() {
    this.uploadButtonName = 'Upload Sertifikat Asisten';
    this.formAsistensi = this.fb.group({
      clt_mk : ['', [Validators.required]],
      clt_periode : ['', [Validators.required]],
      upload: ['', [Validators.required]]
    });
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.progress(true);
      this.layoutDataSharing.changeTitle('Pendaftaran Asisten');
      this.getInitData();
    }, 0);
  }
  getInitData() {
    this.pengalamanAsistenService.initPengalamanAsisten().subscribe(
      res => {
        if (res.status) {
          if (res.data.asistensi.length > 0) {
            this.listAsistensi = res.data.asistensi;
            this.dataSource.data = res.data.asistensi;
          } else {
            this.listAsistensi = null;
          }
          this.periodeAsisten = res.data.periode_asisten;
        } else {
          this.snackBarService.open(res.message, 'OK', 0);
        }
        this.progress(false);
      }
    );
  }
  progress(status: boolean) {
    if (status) {
      this.layoutDataSharing.changeStateProgress(true);
      this.onProgress = true;
      this.formAsistensi.disable();
    } else {
      this.layoutDataSharing.changeStateProgress(false);
      this.formAsistensi.enable();
      this.onProgress = false;
    }
  }
  onFileChange(event) {
    this.file = event.target.files[0];
    if (this.file.size < 2824188) {
      this.uploadButtonName = this.file.name;
    } else {
      this.snackBarService.open('Ukuran gambar terlalu besar, perkecil resolusi gambar', 'OK', 5000);
    }
  }
  onSubmit () {
    if (this.formAsistensi.valid) {
      this.progress(true);
      const uploadData = new FormData();
      uploadData.append('upload', this.file, this.file.name);
      uploadData.append('clt_mk', this.formAsistensi.get('clt_mk').value);
      uploadData.append('clt_periode', this.formAsistensi.get('clt_periode').value);

      this.pengalamanAsistenService.uploadPengalamanAsisten(uploadData).subscribe(
        res => {
          if (res.status) {
            this.snackBarService.open('Berhasil Mengupload Pengalaman Asisten', 'OK', 3000);
            this.formAsistensi.reset();
            this.uploadButtonName = 'Upload Sertifikat Asisten';
            this.getInitData();
          } else {
            this.snackBarService.open(res.message, 'OK', 0);
          }
          // this.listAsistensi = res.data.asistensi;
          this.progress(false);
        }
      );
    } else {
      this.snackBarService.open('Gagal: Lengkapi Form terlebih dahulu!', 'OK', 5000);
    }
  }
  deleteRiwayat (id: string) {
    this.progress(true);
    this.pengalamanAsistenService.deletePengalamanAsisten(id).subscribe(
      res => {
        if (res.status) {
          const index = this.searchIndex(id);
          this.dataSource.data.splice(index, 1);
          this.dataSource = new MatTableDataSource<Asistensi>(this.dataSource.data);
          this.snackBarService.open('Berhasil Menghapus Riwayat', 'OK', 0);
        } else {
          this.snackBarService.open(res.message, 'OK', 0);
        }
        this.progress(false);
      }
    );
  }
  searchIndex (id: string) {
    let index = 0; let i = 0;
    this.listAsistensi.forEach(element => {
      if (element.id === id) {
        index = i;
      }
      i++;
    });
    return index;
  }
}
