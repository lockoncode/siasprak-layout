import { Response } from '../../shared/shared.interface';

/* export interface Organisasi {
    id: string;
    nama: string;
    sk: string;
    periode: string;
    file_url: string;
    tanggal: string;
    keterangan: string;
    status: string;
}*/
export interface Asistensi {
    id: string;
    file_url: string;
    tanggal: string;
    periode: string;
    matakuliah: string;
    keterangan: string;
    status: boolean;
}
export interface PeriodeAsisten {
    id: string;
    label: string;
}
export interface AsistensiResponse extends Response {
    data: {
        asistensi: Asistensi[];
        periode_asisten: PeriodeAsisten[];
    };
}

/* export interface PostOrganisasi {
    clt_nama: string;
    clt_sk: string;
    clt_periode: string;
}
*/
