import { TestBed, inject } from '@angular/core/testing';

import { PengalamanAsistenService } from './pengalaman-asisten.service';

describe('PengalamanAsistenService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PengalamanAsistenService]
    });
  });

  it('should be created', inject([PengalamanAsistenService], (service: PengalamanAsistenService) => {
    expect(service).toBeTruthy();
  }));
});
