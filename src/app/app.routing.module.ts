import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadingStrategy } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { VerifikasiAkunMhsComponent } from './akun/verifikasi-akun-mhs/verifikasi-akun-mhs.component';
import { UbahPasswordAkunComponent } from './akun/ubah-password-akun/ubah-password-akun.component';
import { VerifikasiUbahPasswordComponent } from './akun/verifikasi-ubah-password/verifikasi-ubah-password.component';

import { RegistrasiAkunMhsProtoComponent } from './akun/registrasi-akun-mhs-proto/registrasi-akun-mhs-proto.component';

import { AppPreloader } from './guard/app.preloader';
import { NonAuthGuard } from './guard/non-auth.guard';
import { AuthGuard } from './guard/auth.guard';

const routes: Routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: 'login', component: LoginComponent, canActivate: [NonAuthGuard] },
    {
        path: 'mhs',
        loadChildren: './asisten/asisten.module#AsistenModule',
        data: { preload: 'non-auth' }
    },
    /*{
        path: 'mhs',
        component: AsistenComponent,
        canActivate: [AuthGuard],
        data: { preload: 'auth-asiten'}
    },*/
    {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [AuthGuard],
        data: { preload: 'auth-admin' }
    },
    {
        path: 'daftar-akun',
        component: RegistrasiAkunMhsProtoComponent,
        canActivate: [NonAuthGuard]
    }, {
        path: 'pembaruhan-akun/mhs/:email/:kode_verifikasi',
        component: VerifikasiAkunMhsComponent,
        canActivate: [NonAuthGuard]
    }, {
        path: 'lupa-password',
        component: UbahPasswordAkunComponent,
        canActivate: [NonAuthGuard]
    }, {
        path: 'verifikasi/:mode/:email/:kode_verifikasi',
        component: VerifikasiUbahPasswordComponent
    }, {
        path: '**', redirectTo: 'login'
    }
];

@NgModule({
    imports : [
        RouterModule.forRoot(routes, {
            preloadingStrategy: AppPreloader
        })
    ],
    exports : [
        RouterModule
    ],
    providers : [
        AppPreloader,
        NonAuthGuard,
        AuthGuard
    ]
})
export class AppRoutingModule { }
