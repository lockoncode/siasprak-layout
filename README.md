# SiasprakLayout

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

##Main Layout
http://preview.themeforest.net/item/atomic-angular-4-material-design-admin-framework/full_screen_preview/20833566?_ga=2.116557507.436153828.1525434126-1632131059.1525434126
##Login
https://portal.oxygenna.com/angular/external/login
##notification
http://pd-angular.creative-tim.com/notifications
##Registrasi Akun
http://md-pro-angular.creative-tim.com/forms/wizard
##Data Table + Layout
http://demo.atomicframework.net/tables/atomic-datatable
